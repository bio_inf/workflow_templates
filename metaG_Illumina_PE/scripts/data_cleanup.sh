################################
# metaG workflow data clean-up #
################################

# The below will get rid of intermediate files that are unlikely to be required by any subsequent analyses.
# Use with caution.

# Set paths for data
ADIR="/scratch/usr/mvbchass/Analysis_data/IOWseq000043_EVAR_MSM105_MG_MT"
NAME="v1_water"

# Delete intermediate files from trimming and QC
rm $ADIR/Intermediate_results/$NAME/Adapter_clipped/*_trim1_R1.fastq.gz
rm $ADIR/Intermediate_results/$NAME/Adapter_clipped/*_trim1_R2.fastq.gz
rm $ADIR/Intermediate_results/$NAME/Adapter_clipped/*_trim4_R1.fastq.gz
rm $ADIR/Intermediate_results/$NAME/Adapter_clipped/*_trim4_R2.fastq.gz
rm $ADIR/Intermediate_results/$NAME/Quality_trimmed/*_trim5_R1.fastq
rm $ADIR/Intermediate_results/$NAME/Quality_trimmed/*_trim5_R2.fastq

# Delete concatenated read files
rm $ADIR/Intermediate_results/$NAME/Human_cleaned/all_R1.fastq
rm $ADIR/Intermediate_results/$NAME/Human_cleaned/all_R2.fastq

# Compress human-clean read files
ls -1 $ADIR/Intermediate_results/$NAME/Human_cleaned/final/*.fastq | parallel -j12 gzip

# Delete intermediate files from short read classification
rm -rf $ADIR/Intermediate_results/$NAME/Taxonomy/kraken_out
rm -rf $ADIR/Intermediate_results/$NAME/Taxonomy/kaiju_out

# Delete intermediate files from assemblies
rm -rf $ADIR/Intermediate_results/$NAME/Co_assembly/megahit_metasensitive/intermediate_contigs/
rm -rf $ADIR/Intermediate_results/$NAME/Single_assembly/*_metaspades/K*/
rm -rf $ADIR/Intermediate_results/$NAME/Single_assembly/*_metaspades/misc/
rm -rf $ADIR/Intermediate_results/$NAME/Single_assembly/*_metaspades/corrected/

# Delete intermediate files from binning
rm -rf $ADIR/Intermediate_results/$NAME/Single_binning/*/metawrap_binning/work_files/
rm -rf $ADIR/Intermediate_results/$NAME/Co_binning/metawrap_binning/work_files/

# Delete intermediate files from bin refinement
rm -rf $ADIR/Intermediate_results/$NAME/Single_refinement/*/metawrap_refinement_1/work_files
rm -rf $ADIR/Intermediate_results/$NAME/Single_refinement/*/metawrap_refinement_2/work_files
rm -rf $ADIR/Intermediate_results/$NAME/Co_refinement/metawrap_refinement/work_files

# Delete intermediate files from bin reassembly
rm -rf $ADIR/Intermediate_results/$NAME/Bin_reassembly/tmp_*
rm -rf $ADIR/Intermediate_results/$NAME/Bin_reassembly/reassemblies/*/K*/
rm -rf $ADIR/Intermediate_results/$NAME/Bin_reassembly/reassemblies/*/misc/
rm -rf $ADIR/Intermediate_results/$NAME/Bin_reassembly/reassemblies/*/corrected/

# Delete intermediate files for contig annotation
rm -rf $ADIR/Intermediate_results/$NAME/Contig_annotation/tmp_split/

# Tar directory with a large number of files for data transfer and system-friendly data storage
cd $ADIR/Intermediate_results/$NAME/Bin_final
tar -czvf busco_out.tar.gz busco_out/
tar -czvf gunc_out.tar.gz gunc_out/

cd $ADIR/Intermediate_results/$NAME/Bin_annotation
tar -czvf dbcan_out.tar.gz dbcan_out/
tar -czvf kegg_out.tar.gz kegg_out/
tar -czvf merops_out.tar.gz merops_out/
tar -czvf tcdb_out.tar.gz tcdb_out/


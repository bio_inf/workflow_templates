##########################################
# metaG workflow additional bin curation #
##########################################

# The below represent an example to further improve the bins generated by the metaG workflow

### Set paths for data
# these variables correspond to the following parameters in the workflow configuration:
#   adir
#   analysis
ADIR="/bio/Analysis_data/IOWseq000005_CombiBac"
NAME="v1"


### Manual bin refinement
mkdir $ADIR/Intermediate_results/$NAME/Refinement_anvio

# prepare bins for curation in anvio
ls -1 $ADIR/Intermediate_results/$NAME/Bin_final/bins/*.fa | while read line
do
  bin=$(echo $line | xargs basename | sed -e 's/\.fa//' -e 's/[-\.]/_/g')
  grep '^>' $line | sed -e 's/^>//' -e "s/$/\t${bin}/" 
done > $ADIR/Intermediate_results/$NAME/Refinement_anvio/binning_results.txt

# index bam files (mapping of reads against all bin contigs
cd $ADIR/Intermediate_results/$NAME
conda activate coverm-0.6.1
cat $ADIR/Intermediate_results/$NAME/sample_names.txt | parallel -j5 'samtools index Bin_quantification/{}.bam'
conda deactivate

# create contigs DB
cd $ADIR/Intermediate_results/$NAME/Refinement_anvio
conda activate anvio-7.1
anvi-gen-contigs-database -f ../Bin_quantification/all_contigs.fa -n bin_refinement -T 32 -o contigs.db
# runn HMM
anvi-run-hmms -c contigs.db -T 32

# generate profile databases for each sample
cat $ADIR/Intermediate_results/$NAME/sample_names.txt | while read SID
do
  SAMPLE=$(echo $SID | sed 's/-/_/')
  anvi-profile -i ../Bin_quantification/${SID}.bam -c contigs.db -o ${SAMPLE}_profiles -S ${SAMPLE} -M 1000 -T 32 --min-percent-identity 95
done
# SNV profiling must not be skipped. Will throw error otherwise

# merge profile DBs
anvi-merge *_profiles/PROFILE.db -o SAMPLES-MERGED -c contigs.db -S CombiBac

# add information about binning results (as anvio collection)
anvi-import-collection binning_results.txt -C MAGs -p SAMPLES-MERGED/PROFILE.db -c contigs.db --contigs-mode

# move to interactive anvio
# connect to server via: ssh -L 8080:localhost:8080 bio-40
# select bins of interest to be manually refined
# I recommend to start with bins of high completeness, high coverage, or of specific taxonomic interest
bin="AVHU_4_bin_26_orig" 
anvi-refine -p SAMPLES-MERGED/PROFILE.db -c contigs.db -C MAGs -b $bin --server-only -P 8080
# open in google chrome: http://localhost:8080

# after manual refinement is finished, summarize collection (this is taking forever)
anvi-summarize -p SAMPLES-MERGED/PROFILE.db -c contigs.db -C MAGs -o MERGED-SUMMARY_final


### MIRA reassembly of selected bins of interest
mkdir $ADIR/Intermediate_results/$NAME/Refinement_mira
cd $ADIR/Intermediate_results/$NAME/Refinement_mira
module load mira
conda activate seqtk-1.3

# copy mira manifest template (this is in the config subfolder of this workflow) to analysis folder for each bin
# setting parameters to: -GE:not=4 -NW:cac=warn:cnfs=warn:cmrnl=warn -CO:fnic=yes -AS:nop=6:sdlpo=no -KS:fenn=0.3 -OUT:rtd=yes 
# optionally also specify: -DIR:trt=/work/hassenru/mira_tmp
# the tmp directory should NOT be on a NFS file system

# process bin by bin
# depending on the naming conventions, this could also be run in a loop
bin="AVHU_4_bin_15_strict_1"
sid=$(echo ${bin} | cut -d'_' -f1,2 | sed 's/_/-/')
bid=$(echo ${bin} | cut -d'_' -f4)
mkdir ${bin}
cd ${bin}
# mirabait with permissive reads from reassembly (default kmer)
mirabait -k 31 -b ../../Refinement_anvio/MERGED-SUMMARY_final/bin_by_bin/${bin}/${bin}-contigs.fa -p ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_1.fastq ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_2.fastq -o ./baited_interleaved.fastq &> mirabait.log
# de-interleave reads
seqtk seq baited_interleaved.fastq -1 > baited_r1.fastq
seqtk seq baited_interleaved.fastq -2 > baited_r2.fastq
# run mira
sed "s/XXX/${bin}/" ../manifest_template.conf > manifest.conf
mira manifest.conf &> mira.log

# optionally, downsample high coverage genomes
# this is advisable if mira estimated a coverage of > 200 before
# aim for a coverage of 100-150
conda activate bbmap-38.90
reformat.sh in1=baited_r1.fastq in2=baited_r2.fastq out1=baited_sub_r1.fastq out2=baited_sub_r2.fastq samplerate=0.4
# adjust input read files in mira manifest
mira manifest_sub.conf &> mira_sub.log

# filter out short contigs and those below expected coverage (about 1/2 - 2/3 of average coverage of input assembly) --> look at coverage vs contig length (in R) to decide of best threshold
# also contigs shorter than 1000kb are being excluded here (as in all previous steps of the workflow)
# miraconvert is taking forever... use simple bash tools instead
conda activate bbmap-38.90
bin="AVHU_5_bin_34_orig"
obin="AVHU_5_bin_34_orig"
ls $PWD/${bin}_assembly/${bin}_d_info/${bin}_info_contigstats.txt
sed '1d' ${bin}_assembly/${bin}_d_info/${bin}_info_contigstats.txt | awk '$2 >= 1000 && $6 >= 53' | cut -f1 > contigs_select.txt
filterbyname.sh in=${bin}_assembly/${bin}_d_results/${bin}_out.unpadded.fasta out=${bin}_mira.fasta names=contigs_select.txt include=t
# in case filterbyname does not produce the expected output, the below also works
#   reformat.sh in=${bin}_assembly/${bin}_d_results/${bin}_out.unpadded.fasta out=tmp.fasta fastawrap=100000
#   grep -A1 -w -F -f contigs_select.txt tmp.fasta | sed '/^--$/d' > ${bin}_mira.fasta
# compare with initial assembly
stats.sh in=${bin}_mira.fasta
stats.sh in=../../Refinement_anvio/MERGED-SUMMARY_final/bin_by_bin/${obin}/${obin}-contigs.fa

# running checkm on assemblies before and after mira
cd ..
cp ./*/*_mira.fasta checkm_in/
# also copy anvio refined bins
bin="AVHU_5_bin_2_strict_1"
cp ../Refinement_anvio/MERGED-SUMMARY_final/bin_by_bin/${bin}/${bin}-contigs.fa checkm_in/${bin}_anvio.fasta

mkdir tmp
module load metawrap/1.3.2
conda activate metawrap-env
checkm lineage_wf -x fasta checkm_in checkm_out -t 60 --tmpdir tmp --pplacer_threads 10
/sw/bio/metaWRAP/1.3.2/metaWRAP/bin/metawrap-scripts/summarize_checkm.py checkm_out/storage/bin_stats_ext.tsv | (read -r; printf "%s\n" "$REPLY"; sort) > checkm_out/checkm.stats

# Further inspection with busco (selected bins only)
# run busco AVHU_4_bin_26_orig
conda activate busco-5.2.2
busco -i checkm_in/AVHU_4_bin_26_orig_mira.fasta -o busco_AVHU_4_bin_26_orig_mira -m genome -l flavobacteriales_odb10 -c 60 --offline --download_path /bio/Databases/BUSCO/20210628/busco
# do not hesitate to go back and adjust contig selection (e.g. coverage thresholds)

# Delete mira output if not further used


### Pilon bin polishing
# use permissive reads from reassembly for mapping
# apply to all bins of interest (also those without mira)
# mira assemblies tend to require more corrections with pilon
# for IOW users: this only works on PHY
mkdir $ADIR/Intermediate_results/$NAME/Refinement_pilon/
cd $ADIR/Intermediate_results/$NAME/Refinement_pilon/

# here, we will loop through a list with the bin names that were kept after mira (i.e. decision between anvio or anvio+mira)

# bins taken after anvio
cat pilon_anvio_list.txt | sed '1d' | while read line
do
  echo $line
  # define variables for sample and bin name parsing
  # make sure that this works for your sample and bin names
  sid=$(echo ${line} | cut -d'_' -f1,2 | sed 's/_/-/')
  bid=$(echo ${line} | cut -d'_' -f4)
  # create output directory
  mkdir ${line}
  cd ${line}
  # copy assembly
  cp ../../Refinement_anvio/MERGED-SUMMARY_final/bin_by_bin/${line}/${line}-contigs.fa assembly.fasta
  # map reads
  conda activate coverm-0.6.1
  bwa index assembly.fasta
  bwa mem -t 60 assembly.fasta ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_1.fastq ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_2.fastq | samtools view -b - | samtools sort -@ 60 - > mapped.bam
  samtools index -@ 60 mapped.bam
  conda deactivate
  # run pilon
  conda activate pilon-1.24
  pilon -Xmx512G --genome assembly.fasta --bam mapped.bam --output polished_assembly > pilon.log
  conda deactivate
  cd ..
done

# bins processed with mira
cat pilon_mira_list.txt | while read line
do
  # define variables for sample and bin name parsing
  # make sure that this works for your sample and bin names
  bin=$(echo ${line} | sed 's/_mira//')
  sid=$(echo ${bin} | cut -d'_' -f1,2 | sed 's/_/-/')
  bid=$(echo ${bin} | cut -d'_' -f4)
  # create output directory
  mkdir ${bin}
  cd ${bin}
  # copy assembly
  cp ../../Refinement_mira/${bin}/${line}.fasta assembly.fasta
  # map reads
  conda activate coverm-0.6.1
  bwa index assembly.fasta
  bwa mem -t 60 assembly.fasta ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_1.fastq ../../Bin_reassembly/reads_for_reassembly/${sid}_bin.${bid}.permissive_2.fastq | samtools view -b - | samtools sort -@ 60 - > mapped.bam
  samtools index -@ 60 mapped.bam
  conda deactivate
  # run pilon
  conda activate pilon-1.24
  pilon -Xmx512G --genome assembly.fasta --bam mapped.bam --output polished_assembly > pilon.log
  conda deactivate
  cd ..
done

# collect polished bins
mkdir $ADIR/Intermediate_results/$NAME/Refinement_pilon/polished_bins
ls -1 ./*/polished_assembly.fasta | while read line
do
  bin=$(echo $line | cut -d'/' -f2)
  cp $line polished_bins/$bin.fa
done


### after all manual refinement steps have been completed, repeat bin annoation and assessment (for those that were modified, i.e. in pilon output dir) and quantification
# as a template, the code of the workflow rules in bin_final.smk and bin_annotation.smk can be used


#!/usr/bin/env Rscript

# parse assembly metadata for NCBI genomes

### setting up environment ####

# Check if packages are installed
package.list <- c(
  "crayon",
  "optparse",
  "config",
  "tidyverse",
  "data.table",
  "purrr",
  "janitor"
)

# Function to check if packages are installed
is.installed <- function(pkg){
  is.element(pkg, installed.packages()[,1])
}

# If not all packages are available
if(any(!is.installed(package.list))) {
  cat("Not all required packages are available. They will now be installed.\n")
  
  # give the user the chance to abort manually
  Sys.sleep(20)
  
  # then install packages
  for(i in which(!is.installed(package.list))) {
    suppressMessages(install.packages(package.list[i], repos = "http://cran.us.r-project.org"))
  }
}

# Break the script if the package installation was unsuccessful
if(any(!is.installed(package.list))) {
  cat(
    paste0(
      "Unable to install all required packages.\nPlease install ",
      paste0(package.list[!is.installed(package.list)], collapse = ", "),
      " manually."
    )
  )
  break
}

# Load packages
cat("Loading libraries...")
silent <- suppressMessages(lapply(package.list, function(X) {require(X, character.only = TRUE)}))
rm(silent)
cat(" done\n")

# Some functions for message output
msg <- function(X){
  cat(crayon::white(paste0("[",format(Sys.time(), "%T"), "]")), X)
}
msg_sub <- function(X){
  cat(crayon::white(paste0("  [",format(Sys.time(), "%T"), "]")), X)
}


### Reading command line options ####

# define command line options
option_list <- list(
  make_option(
    c("-i", "--input"),
    type = "character",
    default = NULL,
    help = "space separated list of conifer output files",
    metavar = "character"
  ),
  make_option(
    c("-o", "--output"),
    type = "character",
    default = NULL,
    help = "name of output file",
    metavar = "character"
  ),
  make_option(
    c("-r", "--rtl"),
    type = "double",
    default = 0,
    help = "RTL threshold [default: 0]",
    metavar = "number"
  ),
  make_option(
    c("-c", "--confidence"),
    type = "double",
    default = 0,
    help = "confidence threshold [default: 0]",
    metavar = "number"
  ),
  make_option(
    c("-t", "--threads"),
    type = "double",
    default = 1,
    help = "number of threads [default: 1]",
    metavar = "number"
  )
)

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)

if (is.null(opt$input) | is.null(opt$output)) {
  print_help(opt_parser)
  stop("Provide input and output file names.\n", call. = FALSE)
}


### parse kraken output ####

file_list <- strsplit(opt$input, split = " ")[[1]]

kraken_tax <- map(
  file_list,
  function(x) {
    tmp <- fread(
      x, 
      header = FALSE,
      fill = TRUE, 
      sep = "\t",
      nThread = opt$threads
    )
    colnames(tmp) <- c(
      "state", 
      "seqid", 
      "taxid", 
      "kmers", 
      "scores",
      "confidence_r1",
      "confidence_r2",
      "confidence_avg", 
      "rtl_r1",
      "rtl_r2",
      "rtl_avg",
      "path"
    )
    tmp_filt <- tmp %>% 
      filter(
        confidence_avg >= opt$confidence & rtl_avg >= opt$rtl,
        path != "root",
        !grepl("d__Eukaryota", path)
      ) %>% 
      tabyl(path) %>% 
      select(-3)
    colnames(tmp_filt)[2] <- gsub(".tax", "", basename(x), fixed = T)
    msg(paste0("Finished processing sample ", colnames(tmp_filt)[2], ": there are ", nrow(tmp_filt), " unique taxonomic paths.\n"))
    return(tmp_filt)
  }
) %>% reduce(full_join, by = "path")
# save.image("~/test.Rdata")

kraken_tax_final <- kraken_tax
kraken_tax_final[is.na(kraken_tax_final)] <- 0

write.table(
  kraken_tax_final,
  opt$output,
  quote = F,
  sep = "\t",
  row.names = F
)


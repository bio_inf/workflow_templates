def R1_fun(wildcards):
	return PATH_R1[wildcards.sample]

def R2_fun(wildcards):
	return PATH_R2[wildcards.sample]

rule qc_start:
	input:
		fq_R1 = R1_fun,
		fq_R2 = R2_fun
	output:
		fastqc_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc/{sample}_R1_fastqc.html",
		fastqc_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc/{sample}_R2_fastqc.html"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc"
	threads: 2
	conda: config["wdir"] + "/envs/fastqc.yaml"
	shell:
		"""
		fastqc -t 8 -o {params.outdir} {input.fq_R1} {input.fq_R2}
		SID_R1=$(basename {input.fq_R1} | sed 's/\.fastq\.gz//')
		SID_R2=$(basename {input.fq_R2} | sed 's/\.fastq\.gz//')
		mv {params.outdir}/${{SID_R1}}_fastqc.html {output.fastqc_R1}
		mv {params.outdir}/${{SID_R2}}_fastqc.html {output.fastqc_R2}
		"""

rule remove_phix:
	input:
		fq_R1 = R1_fun,
		fq_R2 = R2_fun
	output:
		trim1_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1_R1.fastq.gz",
		trim1_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1_R2.fastq.gz",
		stats1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_stats1.txt"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/bbmap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1.log"
	shell:
		"""
		bbduk.sh in={input.fq_R1} in2={input.fq_R2} out={output.trim1_R1} out2={output.trim1_R2} ref=phix k=28 stats={output.stats1} threads={threads} fastawrap=300 > {log} 2>&1
		"""

rule remove_adapter_right:
	input:
		trim1_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1_R1.fastq.gz",
		trim1_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1_R2.fastq.gz",
	output:
		trim3_R1 = config["adir"] + "/Validated_data/{sample}_trim3_R1.fastq.gz",
		trim3_R2 = config["adir"] + "/Validated_data/{sample}_trim3_R2.fastq.gz",
		stats3 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_stats3.txt"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/bbmap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim3.log"
	shell:
		"""
		bbduk.sh in={input.trim1_R1} in2={input.trim1_R2} out={output.trim3_R1} out2={output.trim3_R2} ref=adapters k=23 mink=11 ktrim=r hdist=1 stats={output.stats3} threads={threads} tpe fastawrap=300 > {log} 2>&1
		"""

# the below settings for the removal of optical duplicates are optimized for NovaSeq data
# they need to be adapted if the reads were generated on a different platform
rule remove_optical_dups:
	input:
		trim3_R1 = config["adir"] + "/Validated_data/{sample}_trim3_R1.fastq.gz",
		trim3_R2 = config["adir"] + "/Validated_data/{sample}_trim3_R2.fastq.gz"
	output:
		trim4_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4_R1.fastq.gz",
		trim4_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4_R2.fastq.gz"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/bbmap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4.log"
	shell:
		"""
		clumpify.sh in={input.trim3_R1} in2={input.trim3_R2} out={output.trim4_R1} out2={output.trim4_R2} dedupe=t optical=t dist=12000 subs=2 threads={threads} fastawrap=300 > {log} 2>&1
		"""

rule quality_trimming:
	input:
		trim4_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4_R1.fastq.gz",
		trim4_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4_R2.fastq.gz"
	output:
		trim5_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R1.fastq",
		trim5_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R2.fastq"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/bbmap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5.log"
	shell:
		"""
		bbduk.sh in={input.trim4_R1} in2={input.trim4_R2} out={output.trim5_R1} out2={output.trim5_R2} qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 threads={threads} fastawrap=300 > {log} 2>&1
		"""

rule qc_end:
	input:
		trim5_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R1.fastq",
		trim5_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R2.fastq"
	output:
		fastqc_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc/{sample}_trim5_R1_fastqc.html",
		fastqc_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc/{sample}_trim5_R2_fastqc.html"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Fastqc"
	threads: 2
	conda: config["wdir"] + "/envs/fastqc.yaml"
	shell:
		"""
		fastqc -t 8 -o {params.outdir} {input.trim5_R1} {input.trim5_R2}
		"""

rule remove_human:
	input:
		trim5_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R1.fastq",
		trim5_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5_R2.fastq"
	output:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/{sample}"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/{sample}_bmtagger.log"
	shell:
		"""
		metaWRAP read_qc --skip-trimming --skip-pre-qc-report --skip-post-qc-report -1 {input.trim5_R1} -2 {input.trim5_R2} -o {params.outdir} -t {threads} > {log} 2>&1
		mv {params.outdir}/final_pure_reads_1.fastq {output.clean_R1}
		mv {params.outdir}/final_pure_reads_2.fastq {output.clean_R2}
		"""

rule collect_nseq:
	input:
		trim1 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim1.log", sample = SAMPLES),
		trim3 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim3.log", sample = SAMPLES),
		trim4 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Adapter_clipped/{sample}_trim4.log", sample = SAMPLES),
		trim5 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Quality_trimmed/{sample}_trim5.log", sample = SAMPLES),
		clean_fq = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq", sample = SAMPLES)
	output:
		nseq = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/nSeqs_qc_" + config["project"] + ".txt"
	params:
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/tmp",
		sample_list = config["sample_list"]
	shell:
		"""
		mkdir -p {params.tmpdir}
		grep '^Input:' {input.trim1} | cut -f2 | sed 's/ reads //' | awk '{{print $1/2}}' | paste <(cut -f1 {params.sample_list}) - > {params.tmpdir}/tmp1
		grep '^Result:' {input.trim1} | cut -f2 | sed 's/ reads.*//' | awk '{{print $1/2}}' | paste {params.tmpdir}/tmp1 - > {params.tmpdir}/tmp2
		grep '^Result:' {input.trim3} | cut -f2 | sed 's/ reads.*//' | awk '{{print $1/2}}' | paste {params.tmpdir}/tmp2 - > {params.tmpdir}/tmp3
		grep 'Reads Out:' {input.trim4} | sed 's/\s\+/\t/g' | cut -f3 | awk '{{print $1/2}}' | paste {params.tmpdir}/tmp3 - > {params.tmpdir}/tmp4
		grep '^Result:' {input.trim5} | cut -f2 | sed 's/ reads.*//' | awk '{{print $1/2}}' | paste {params.tmpdir}/tmp4 - > {params.tmpdir}/tmp5
		echo "{input.clean_fq}" | xargs wc -l | sed '$d' | awk '{{print $1/4}}' | paste {params.tmpdir}/tmp5 - > {params.tmpdir}/tmp6
		echo -e 'SID\\toriginal\\ttrim_phix\\ttrim_right\\tdeduplicated\\ttrim_quality\\thuman_cleaned' | cat - {params.tmpdir}/tmp6 > {output.nseq}
		# rm {params.tmpdir}/tmp*
		"""


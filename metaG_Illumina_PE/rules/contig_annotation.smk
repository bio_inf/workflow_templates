# consider reducing min ORF length for more sensitive annotation (at the moment only proteins with more than 60aa are considered)
rule metaerg_contigs:
	input:
		assembly = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final_assembly.fa"
	output:
		html = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/index.html",
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa",
		master =  config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/master.tsv.txt",
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/all.gff"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metaerg.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg.log"
	shell:
		"""
		metaerg.pl --gtype meta --sp --tm --minorflen 180 --prefix contig --outdir {params.outdir}/metaerg_tmp --cpus {threads} {input.assembly} >> {log} 2>&1
		mv {params.outdir}/metaerg_tmp/* {params.outdir}/
		cd {params.outdir}
		tar -xzvf tmp.tar.gz tmp/
		rmdir metaerg_tmp
		"""

rule diamond_kegg_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_kegg.txt"
	params:
		kegg_dmnd = config["kegg_diamond_db"],
		evalue = config["evalue"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	threads: config["parallel_threads"]
	shell:
		"""
		diamond blastp -d {params.kegg_dmnd} -b 8 --sensitive -e {params.evalue} -q {input.cds} --top 10 -o {output.blast_kegg} -f 6 -p {threads}
		"""

rule diamond_self_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_self_diamond.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond makedb --in {input.cds} -d {params.outdir}/cds_db -p {threads}
		diamond blastp -d {params.outdir}/cds_db.dmnd -b 4 -k 1 --query {input.cds} -o {output.blast_self} -f 6 -p {threads}
		rm {params.outdir}/cds_db.dmnd
		"""

rule parse_kegg_contigs:
	input:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_kegg.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_self_diamond.txt"
	output:
		kegg_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_kegg_parsed.txt",
		kegg_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_kegg_parsed_best.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out",
		script = config["wdir"] + "/scripts/parse_kegg.R",
		mapdir = config["kegg_mapdir"],
		bsr = config["bsr"]
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.blast_kegg} -s {input.blast_self} -m {params.mapdir} -c {params.bsr} -t {threads} -o {params.outdir}/out_kegg_parsed
		"""

checkpoint split_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa",
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/all.gff"
	output:
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/tmp.gff",
		split_dir = directory(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/splits")
	params:
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split"
	conda: config["wdir"] + "/envs/kraken2.yaml"
	shell:
		"""
		mkdir -p {params.tmpdir}
		cd {params.tmpdir}
		seqkit split2 -1 {input.cds} -p 100 -O splits -f
		sed '/^\\#\\#FASTA$/,$d' {input.gff} > {output.gff}
		"""

rule merops_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		scan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_scan.txt",
		pepunit = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_pepunit.txt"
	params:
		scan_dmnd = config["merops_scan"],
		pepunit_dmnd = config["merops_pepunit"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/merops_diamond.log"
	shell:
		"""
		diamond blastp -d {params.scan_dmnd} --sensitive -e {params.evalue} -q {input.cds} -k 1 -p {threads} -o {output.scan} -f 6
		diamond blastp -d {params.pepunit_dmnd} --sensitive -e {params.evalue} -q {input.cds} --top 10 -p {threads} -o {output.pepunit} -f 6
		"""

rule parse_merops_contigs:
	input:
		scan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_scan.txt",
		pepunit = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_pepunit.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_self_diamond.txt"
	output:
		merops = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_parsed.txt",
		merops_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out/out_merops_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_merops.R",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/merops_out",
		metadata_pepunit = config["merops_meta_pepunit"],
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -f {input.scan} -p {input.pepunit} -m {params.metadata_pepunit} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_merops_parsed
		"""

rule dbcan_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/splits/{split}.faa",
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/tmp.gff"
	output:
		dbcan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/dbcan_out/{split}/overview.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/dbcan_out",
		dbdir = config["dbcan_db"],
		split = "{split}"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/dbcan_out/logs/{split}_dbcan.log"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		grep '^>' {input.cds} | sed -e 's/^>//' -e 's/ .*//' | grep -w -F -f - {input.gff} > {params.split}.gff
		run_dbcan {input.cds} protein -c {params.split}.gff --out_dir {params.split} --db_dir {params.dbdir} >> {log} 2>&1
		rm {params.split}.gff
		"""

rule tcdb_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		tcdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tcdb_out/out_tcdb_diamond.txt"
	params:
		dmnd = config["tcdb_dmnd"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.dmnd} --sensitive -e {params.evalue} -q {input.cds} --top 10 -p {threads} -o {output.tcdb} -f 6
		"""

rule parse_tcdb_contigs:
	input:
		tcdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tcdb_out/out_tcdb_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_self_diamond.txt"
	output:
		tcdb_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tcdb_out/out_tcdb_parsed.txt",
		tcdb_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tcdb_out/out_tcdb_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_tcdb.R",
		metadir = config["tcdb_metadir"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tcdb_out",
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.tcdb} -m {params.metadir} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_tcdb_parsed
		"""

rule ncycdb_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		ncycdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/ncycdb_out/out_ncycdb_diamond.txt"
	params:
		dmnd = config["ncycdb_dmnd"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.dmnd} --sensitive -e {params.evalue} -q {input.cds} --top 10 -p {threads} -o {output.ncycdb} -f 6
		"""

rule parse_ncycdb_contigs:
	input:
		ncycdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/ncycdb_out/out_ncycdb_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kegg_out/out_self_diamond.txt"
	output:
		ncycdb_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/ncycdb_out/out_ncycdb_parsed.txt",
		ncycdb_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/ncycdb_out/out_ncycdb_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_ncycdb.R",
		metafile = config["ncycdb_metafile"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/ncycdb",
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.ncycdb} -m {params.metafile} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_ncycdb_parsed
		"""

# The e-value setting is taken from the default for kofamscan on the web interface
rule kofam_contigs:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		kofam_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kofam_out/out_kofamscan.txt",
		kofam_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kofam_out/out_kofam_parsed.txt"
	params:
		kofam_db = config["kofam_db"],
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kofam_out/tmp",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/kofam_out/"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/kofamscan.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		exec_annotation -o {output.kofam_out} -p {params.kofam_db}/profiles -k {params.kofam_db}/ko_list --tmp-dir={params.tmpdir} --cpu {threads} -E 0.01 {input.cds}
		grep "^\\*" {output.kofam_out} | sed 's/^\\* //' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sort -k1,1 -k4,4gr -k5,5g | sort -u -k1,1 --merge > {output.kofam_parsed}
		rm -rf {params.tmpdir}
		"""

def aggregate_dbcan_splits(wildcards):
	checkpoint_output = checkpoints.split_contigs.get().output['split_dir']
	fasta_files = sorted(glob.glob(os.path.join(checkpoint_output, '*.faa')))
	split = [re.sub('\.faa$', '', os.path.basename(x)) for x in fasta_files]
	return expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/tmp_split/dbcan_out/{split}/overview.txt", split = split)

rule collect_contig_splits:
	input:
		dbcan = aggregate_dbcan_splits
	output:
		dbcan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/dbcan_out/overview.txt"
	shell:
		"""
		echo -e "Gene ID\\tEC#\\tHMMER\\teCAMI\\tDIAMOND\\t#ofTools" > {output.dbcan}
		for i in {input.dbcan}
		do
		  sed '1d' $i >> {output.dbcan}
		done
		"""

# the e-value setting is taken from the default for CAT/BAT
rule cross_domain_diamond:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/cds.faa"
	output:
		cds_cat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/cds.CAT.faa",
		diamond_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/cds.CAT.alignment.diamond"
	params:
		cat_dmnd = config["cat_cd_dmnd"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		sed -E '/^>/s/metaerg\\.pl\\|([0-9]+) .*cid=(.*)$/\\2_\\1/' {input.cds} > {output.cds_cat}
		diamond blastp -d {params.cat_dmnd} -b 8 -e 0.001 -q {output.cds_cat} --top 11 -o {output.diamond_out} -p {threads}
		"""

rule cat_contigs:
	input:
		assembly = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final_assembly.fa",
		cds_cat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/cds.CAT.faa",
		diamond_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/cds.CAT.alignment.diamond"
	output:
		orf2lca = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/cds.CAT.ORF2LCA.txt",
		classified = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out/contig_classification_cross_domain.txt"
	params:
		dbdir = config["cat_cd_db"],
		taxdir = config["cat_cd_tax"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/cat_out",
		cat_r = config["cat_r"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/cat.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		CAT contigs -c {input.assembly} -d {params.dbdir} -t {params.taxdir} -p {input.cds_cat} -a {input.diamond_out} -n {threads} --out_prefix cds.CAT -r {params.cat_r} --I_know_what_Im_doing --top 11
		CAT add_names -i cds.CAT.contig2classification.txt -o {output.classified} -t {params.taxdir} --exclude_scores
		"""

# maximum insert size is set to 1000 here (instead of 600) to allow for libraries with larger inserts
# it seems unlikely, that this parameter needs to be changed for Illumina data sets 
rule gene_counts_contigs:
	input:
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/metaerg_out/data/all.gff",
		mapstats = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/{sample}_mapstats.txt", sample = SAMPLES)
	output:
		gff_clean = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/gene_counts/genes.gff",
		gene_counts = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/gene_counts/genes_counts.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/work_files"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/subread.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/gene_counts/feature_counts.log"
	shell:
		"""
		sed '/^\\#\\#FASTA$/,$d' {input.gff} | sed 's/;.*$//' > {output.gff_clean}
		featureCounts -t CDS -g ID -p -D 1000 --minOverlap 50 -T {threads} -a {output.gff_clean} -o {output.gene_counts} {params.bamdir}/*.bam > {log} 2>&1 
		"""

rule contig_quantification:
	input:
		mapstats = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/{sample}_mapstats.txt", sample = SAMPLES)
	output:
		tpm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/contig_quantification/contig_tpm_coverm.txt",
		mean = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/contig_quantification/contig_mean_coverm.txt",
		contig_count = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Contig_annotation/contig_quantification/contig_count_coverm.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/work_files"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		coverm contig -b {params.bamdir}/*.bam -m tpm --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -t {threads} > {output.tpm}
		coverm contig -b {params.bamdir}/*.bam -m mean --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -t {threads} > {output.mean}
		coverm contig -b {params.bamdir}/*.bam -m count --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -t {threads} > {output.contig_count}
		"""

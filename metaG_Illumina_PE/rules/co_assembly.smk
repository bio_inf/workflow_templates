rule assembly_megahit:
	input:
		clean_R1 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq", sample = SAMPLES),
		clean_R2 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq", sample = SAMPLES)
	output:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final.contigs.fa"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/megahit.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive.log"
	shell:
		"""
		echo "{input.clean_R1}" | tr ' ' ',' > {params.outdir}/R1.csv
		echo "{input.clean_R2}" | tr ' ' ',' > {params.outdir}/R2.csv
		megahit --presets meta-sensitive -1 $(<{params.outdir}/R1.csv) -2 $(<{params.outdir}/R2.csv) -t {threads} -m 0.98 --min-contig-len 1000 --continue -o {params.outdir}/megahit_metasensitive/tmp > {log} 2>&1
		mv {params.outdir}/megahit_metasensitive/tmp/* {params.outdir}/megahit_metasensitive/
		"""

rule co_assembly_stats:
	input:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final.contigs.fa"
	output:
		stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final.contigs.stats"
	conda: config["wdir"] + "/envs/bbmap.yaml"
	shell:
		"""
		stats.sh in={input.contigs} > {output.stats}
		"""

rule megahit_fix_naming:
	input:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final.contigs.fa"
	output:
		assembly = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final_assembly.fa"
	params:
		script_dir = config["metawrap_scripts"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	shell:
		"""
		{params.script_dir}/fix_megahit_contig_naming.py {input.contigs} 1000 > {output.assembly}
		"""

rule co_assembly_binning:
	input:
		clean_R1 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq", sample = SAMPLES),
		clean_R2 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq", sample = SAMPLES),
		assembly = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final_assembly.fa"
	output:
		depth = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/work_files/metabat_depth.txt",
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/bin.unbinned.fa"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning.log"
	shell:
		"""
		echo $OMP_NUM_THREADS
		OMP_THREAD_LIMIT=192
		echo $OMP_THREAD_LIMIT
		metawrap binning --metabat2 --concoct -t {threads} -l 1000 -m 350 -a {input.assembly} -o {params.outdir} {params.indir}/*.fastq > {log} 2>&1
		mv {params.outdir}/concoct_bins/unbinned.fa {output.unbinned_concoct}
		mv {params.outdir}/metabat2_bins/bin.unbinned.fa {output.unbinned_metabat2}
		"""

rule co_mapping_stats:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/bin.unbinned.fa"
	output:
		mapstats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/{sample}_mapstats.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/work_files",
		sample = "{sample}"
	conda: config["wdir"] + "/envs/metawrap.yaml"
	shell:
		"""
		samtools flagstat {params.bamdir}/{params.sample}.bam > {output.mapstats}
		"""

# see sample_assembly.smk for explanation of parameter settings
rule co_assembly_refinement:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/bin.unbinned.fa"
	output:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/metawrap_refinement/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/metawrap_refinement",
		comp = config["completeness"],
		cont = config["contamination"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/metawrap_refinement.log"
	shell:
		"""
		metawrap bin_refinement -o {params.outdir} -t {threads} -m 370 -c {params.comp} -x {params.cont} -A {params.indir}/metabat2_bins -B {params.indir}/concoct_bins > {log} 2>&1
		"""

rule quant_bins_co:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/bin.unbinned.fa",
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/metawrap_refinement/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats"
	output:
		co_quant = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/Co_bins_quant.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_binning/metawrap_binning/work_files",
		bindir =  config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_refinement/metawrap_refinement/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins",
		pident = config["read_identity"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		coverm genome -b {params.bamdir}/*.bam -m mean --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -d {params.bindir} -x fa -t {threads} > {output.co_quant}
		"""

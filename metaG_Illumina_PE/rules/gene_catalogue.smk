rule prodigal_sample_assembly:
	input:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta"
	output:
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}.gff",
		protein_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}_protein.faa",
		protein_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}_protein.fna"
	params:
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp",
		sample = "{sample}"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/pprodigal.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}_prodigal.log"
	shell:
		"""
		mkdir -p {params.tmpdir}
		export TMP="{params.tmpdir}"
		export TMPDIR="{params.tmpdir}"
		sed 's/^>/>{params.sample}_/' {input.contigs} | seqkit seq -m 1000 | pprodigal -p meta -o {output.gff} -a {output.protein_faa} -d {output.protein_fna} -f gff -T {threads} >> {log} 2>&1
		"""

rule prodigal_co_assembly:
	input:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Co_assembly/megahit_metasensitive/final_assembly.fa"
	output:
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co.gff",
		protein_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co_protein.faa",
		protein_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co_protein.fna"
	params:
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/pprodigal.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co_prodigal.log"
	shell:
		"""
		mkdir -p {params.tmpdir}
		export TMP="{params.tmpdir}"
		export TMPDIR="{params.tmpdir}"
		sed 's/^>/>Co_/' {input.contigs} | pprodigal -p meta -o {output.gff} -a {output.protein_faa} -d {output.protein_fna} -f gff -T {threads} >> {log} 2>&1
		"""

rule concat_protein:
	input:
		sample_faa = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}_protein.faa", sample = SAMPLES),
		sample_fna = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/{sample}_protein.fna", sample = SAMPLES),
		co_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co_protein.faa",
		co_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/predicted/co_protein.fna"
	output:
		concat_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein.faa",
		concat_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein.fna"
	shell:
		"""
		cat {input.sample_faa} {input.co_faa} > {output.concat_faa}
		cat {input.sample_fna} {input.co_fna} > {output.concat_fna}
		"""

# settings taken from BAGS paper
rule cluster_genes:
	input:
		concat_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein.faa"
	output:
		cluster_reps = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq.fasta",
		cluster_tsv = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_cluster.tsv"
	params:
		out_prefix = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue",
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/tmp"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/mmseqs2.yaml"
	shell:
		"""
		mmseqs easy-cluster {input.concat_faa} {params.out_prefix} {params.tmpdir} -c 0.9 --min-seq-id 0.95 --cov-mode 1 --cluster-mode 2 --threads {threads}
		"""

rule filter_cluster_reps:
	input:
		cluster_reps = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq.fasta"
	output:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	conda: config["wdir"] + "/envs/pprodigal.yaml"
	shell:
		"""
		seqkit seq -m 60 -w 0 {input.cluster_reps} > {output.cluster_filt}
		"""

rule extract_catalogue_fna:
	input:
		concat_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein.fna",
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		reps_accnos = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.accnos",
		reps_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna"
	conda: config["wdir"] + "/envs/bbmap.yaml"
	shell:
		"""
		grep '^>' {input.cluster_filt} | sed -e 's/^>//' -e 's/ .*//' > {output.reps_accnos}
		filterbyname.sh in={input.concat_fna} out={output.reps_fna} names={output.reps_accnos} include=t
		"""

rule extract_catalogue_gff:
	input:
		reps_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna"
	output:
		reps_gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.gff"
	params:
		script = config["gff_script"]
	shell:
		"""
		python {params.script} -i {input.reps_fna} -o {output.reps_gff}
		"""

rule index_gene_catalogue:
	input:
		reps_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna"
	output:
		index = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna.bwt"
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		bwa index {input.reps_fna}
		"""

rule mapping_gene_catalogue:
	input:
		reps_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna",
		index = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna.bwt",
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		bam = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified/{sample}.bam"
	params:
		sample = "{sample}",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		bwa mem -v 1 -t {threads} {input.reps_fna} {input.clean_R1} {input.clean_R2} | samtools sort -T {params.outdir}/tmp_{params.sample}_samtools -@ {threads} -O BAM > {output.bam}
		"""

# at the moment 50bp of read need to overlap feature to count it. maybe adjust of make user-configurable in the future
rule quant_gene_catalogue:
	input:
		bam = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified/{sample}.bam", sample = SAMPLES),
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.gff"
	output:
		counts = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified/genes_counts.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/subread.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/quantified/feature_counts.log"
	shell:
		"""
		featureCounts -t CDS -g ID -p -D 1000 --minOverlap 50 -T {threads} --maxMOp 50 -a {input.gff} -o {output.counts} {params.bamdir}/*.bam > {log} 2>&1
		"""

# Gene classification using CAT LCA parsing to get a consensus based on all genes per cluster
rule prepare_cat_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa",
		concat_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein.faa",
		cluster_tsv = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_cluster.tsv"
	output:
		cluster_tsv_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_cluster_filt.tsv",
		concat_faa_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/all_protein_filt.faa",
		cds_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.faa"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/pprodigal.yaml"
	shell:
		"""
		# extract members of filtered clusters from cluster file
		grep '^>' {input.cluster_filt} | sed -e 's/^>//' -e 's/ .*$//' | grep -w -F -f - {input.cluster_tsv} > {output.cluster_tsv_filt}
		# extract member sequences by name
		cut -f2 {output.cluster_tsv_filt} | seqkit grep -f - {input.concat_faa} | seqkit replace -p "\\s.+" > {output.concat_faa_filt}
		# rename sequences
		paste <(cut -f2 {output.cluster_tsv_filt}) <(cut -f1 {output.cluster_tsv_filt}) | seqkit replace -p "(.+)" -r "{{kv}}_{{nr}}" -k - -j {threads} {output.concat_faa_filt} > {output.cds_faa}
		"""

rule cross_domain_diamond_gene_catalogue:
	input:
		cds_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.faa",
	output:
		diamond_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.alignment.diamond"
	params:
		cat_dmnd = config["cat_cd_dmnd"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.cat_dmnd} -b 8 -e 0.001 -q {input.cds_faa} --top 6 -o {output.diamond_out} -p {threads}
		"""

rule cat_gene_catalogue:
	input:
		reps_fna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.fna",
		cds_faa = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.faa",
		diamond_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.alignment.diamond"
	output:
		orf2lca = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue.CAT.ORF2LCA.txt",
		classified = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified/catalogue_classification_cross_domain.txt"
	params:
		dbdir = config["cat_cd_db"],
		taxdir = config["cat_cd_tax"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/classified"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/cat.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		CAT contigs -c {input.reps_fna} -d {params.dbdir} -t {params.taxdir} -p {input.cds_faa} -a {input.diamond_out} -n {threads} --out_prefix catalogue.CAT -r 3 -f 0.5 --I_know_what_Im_doing --top 6
		CAT add_names -i catalogue.CAT.contig2classification.txt -o {output.classified} -t {params.taxdir} --exclude_scores
		"""

# Gene annotation
rule kegg_diamond_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_kegg.txt"
	params:
		kegg_dmnd = config["kegg_diamond_db"],
		evalue = config["evalue"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	threads: config["parallel_threads"]
	shell:
		"""
		diamond blastp -d {params.kegg_dmnd} -b 8 --sensitive -e {params.evalue} -q {input.cluster_filt} --top 10 -o {output.blast_kegg} -f 6 -p {threads}
		"""

rule diamond_self_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_self_diamond.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond makedb --in {input.cluster_filt} -d {params.outdir}/cds_db -p {threads}
		diamond blastp -d {params.outdir}/cds_db.dmnd -b 4 -k 1 --query {input.cluster_filt} -o {output.blast_self} -f 6 -p {threads}
		rm {params.outdir}/cds_db.dmnd
		"""

rule parse_kegg_gene_catalogue:
	input:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_kegg.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_self_diamond.txt"
	output:
		kegg_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_kegg_parsed.txt",
		kegg_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_kegg_parsed_best.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out",
		script = config["wdir"] + "/scripts/parse_kegg.R",
		mapdir = config["kegg_mapdir"],
		bsr = config["bsr"]
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.blast_kegg} -s {input.blast_self} -m {params.mapdir} -c {params.bsr} -t {threads} -o {params.outdir}/out_kegg_parsed
		"""

rule cog_diamond_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		blast_cog = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/cog_out/out_cog.txt"
	params:
		cog_dmnd = config["cog_diamond_db"],
		evalue = config["evalue"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	threads: config["parallel_threads"]
	shell:
		"""
		diamond blastp -d {params.cog_dmnd} -b 8 --sensitive -e {params.evalue} -q {input.cluster_filt} --top 10 -o {output.blast_cog} -f 6 -p {threads}
		"""

checkpoint split_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		split_dir = directory(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/splits")
	params:
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split"
	conda: config["wdir"] + "/envs/kraken2.yaml"
	shell:
		"""
		mkdir -p {params.tmpdir}
		cd {params.tmpdir}
		seqkit split2 -1 {input.cluster_filt} -p 100 -O splits -f
		"""

rule dbcan_gene_catalogue:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/splits/{split}.faa",
		reps_gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.gff"
	output:
		dbcan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/dbcan_out/{split}/overview.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/dbcan_out",
		dbdir = config["dbcan_db"],
		split = "{split}"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/dbcan_out/logs/{split}_dbcan.log"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		grep '^>' {input.cds} | sed -e 's/^>//' -e 's/ .*//' | grep -w -F -f - {input.gff} > {params.split}.gff
		run_dbcan {input.cds} protein -c {params.split}.gff --out_dir {params.split} --db_dir {params.dbdir} >> {log} 2>&1
		rm {params.split}.gff
		"""

def aggregate_dbcan_splits_gene_catalogue(wildcards):
	checkpoint_output = checkpoints.split_contigs.get().output['split_dir']
	fasta_files = sorted(glob.glob(os.path.join(checkpoint_output, '*.faa')))
	split = [re.sub('\.faa$', '', os.path.basename(x)) for x in fasta_files]
	return expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tmp_split/dbcan_out/{split}/overview.txt", split = split)

rule collect_gene_catalogue_splits:
	input:
		dbcan = aggregate_dbcan_splits_gene_catalogue
	output:
		dbcan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/dbcan_out/overview.txt"
	shell:
		"""
		echo -e "Gene ID\\tEC#\\tHMMER\\teCAMI\\tDIAMOND\\t#ofTools" > {output.dbcan}
		for i in {input.dbcan}
		do
		  sed '1d' $i >> {output.dbcan}
		done
		"""

rule merops_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		scan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_scan.txt",
		pepunit = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_pepunit.txt"
	params:
		scan_dmnd = config["merops_scan"],
		pepunit_dmnd = config["merops_pepunit"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/merops_diamond.log"
	shell:
		"""
		diamond blastp -d {params.scan_dmnd} --sensitive -e {params.evalue} -q {input.cluster_filt} -k 1 -p {threads} -o {output.scan} -f 6
		diamond blastp -d {params.pepunit_dmnd} --sensitive -e {params.evalue} -q {input.cluster_filt} --top 10 -p {threads} -o {output.pepunit} -f 6
		"""

rule parse_merops_gene_catalogue:
	input:
		scan = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_scan.txt",
		pepunit = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_pepunit.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_self_diamond.txt"
	output:
		merops = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_parsed.txt",
		merops_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out/out_merops_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_merops.R",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/merops_out",
		metadata_pepunit = config["merops_meta_pepunit"],
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -f {input.scan} -p {input.pepunit} -m {params.metadata_pepunit} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_merops_parsed
		"""

rule tcdb_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		tcdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tcdb_out/out_tcdb_diamond.txt"
	params:
		dmnd = config["tcdb_dmnd"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.dmnd} --sensitive -e {params.evalue} -q {input.cluster_filt} --top 10 -p {threads} -o {output.tcdb} -f 6
		"""

rule parse_tcdb_gene_catalogue:
	input:
		tcdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tcdb_out/out_tcdb_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_self_diamond.txt"
	output:
		tcdb_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tcdb_out/out_tcdb_parsed.txt",
		tcdb_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tcdb_out/out_tcdb_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_tcdb.R",
		metadir = config["tcdb_metadir"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/tcdb_out",
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.tcdb} -m {params.metadir} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_tcdb_parsed
		"""

rule ncycdb_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		ncycdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/ncycdb_out/out_ncycdb_diamond.txt"
	params:
		dmnd = config["ncycdb_dmnd"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.dmnd} --sensitive -e {params.evalue} -q {input.cluster_filt} --top 10 -p {threads} -o {output.ncycdb} -f 6
		"""

rule parse_ncycdb_gene_catalogue:
	input:
		ncycdb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/ncycdb_out/out_ncycdb_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kegg_out/out_self_diamond.txt"
	output:
		ncycdb_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/ncycdb_out/out_ncycdb_parsed.txt",
		ncycdb_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/ncycdb_out/out_ncycdb_parsed_best.txt"
	params:
		script = config["wdir"] + "/scripts/parse_ncycdb.R",
		metafile = config["ncycdb_metafile"],
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/ncycdb",
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.ncycdb} -m {params.metafile} -s {input.blast_self} -c {params.bsr} -o {params.outdir}/out_ncycdb_parsed
		"""

# The e-value setting is taken from the default for kofamscan on the web interface
rule kofam_gene_catalogue:
	input:
		cluster_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/clustered/catalogue_rep_seq_filt.faa"
	output:
		kofam_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kofam_out/out_kofamscan.txt",
		kofam_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kofam_out/out_kofam_parsed.txt"
	params:
		kofam_db = config["kofam_db"],
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kofam_out/tmp",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Gene_catalogue/kofam_out/"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/kofamscan.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		exec_annotation -o {output.kofam_out} -p {params.kofam_db}/profiles -k {params.kofam_db}/ko_list --tmp-dir={params.tmpdir} --cpu {threads} -E 0.01 {input.cluster_filt}
		grep "^\\*" {output.kofam_out} | sed 's/^\\* //' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sort -k1,1 -k4,4gr -k5,5g | sort -u -k1,1 --merge > {output.kofam_parsed}
		rm -rf {params.tmpdir}
		"""



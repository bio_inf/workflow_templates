rule assembly_metaspades:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta",
		graph = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/assembly_graph_with_scaffolds.gfa",
		paths = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.paths"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/spades.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades.log"
	shell:
		"""
		spades.py -o {params.outdir} --meta -1 {input.clean_R1} -2 {input.clean_R2} -t {threads} -m 370 -k 21,33,55,77,99,127 > {log} 2>&1
		"""

rule single_assembly_stats:
	input:
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta"
	output:
		stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.stats"
	conda: config["wdir"] + "/envs/bbmap.yaml"
	shell:
		"""
		stats.sh in={input.contigs} > {output.stats}
		"""

rule single_assembly_binning:
	input:
		clean_R1 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sid}_1.fastq", sid = SID),
		clean_R2 = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sid}_2.fastq", sid = SID),
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta"
	output:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/bin.unbinned.fa"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning.log"
	shell:
		"""
		echo $OMP_NUM_THREADS
		metawrap binning --metabat2 --maxbin2 --concoct -t {threads} -l 1000 -m 350 -a {input.contigs} -o {params.outdir} {params.indir}/*.fastq > {log} 2>&1
		mv {params.outdir}/concoct_bins/unbinned.fa {output.unbinned_concoct}
		mv {params.outdir}/metabat2_bins/bin.unbinned.fa {output.unbinned_metabat2}
		"""

rule single_mapping_stats:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/bin.unbinned.fa"
	output:
		mapstats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/{sid}_mapstats.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/work_files",
		sid = "{sid}"
	conda: config["wdir"] + "/envs/metawrap.yaml"
	shell:
		"""
		samtools flagstat {params.bamdir}/{params.sid}.bam > {output.mapstats}
		"""

rule metacoag_coverage:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/bin.unbinned.fa"
	output:
		coverage = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/abundance.tsv",
		noheader = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/abundance_metacoag.tsv"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/work_files"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		coverm contig -b {params.indir}/*.bam -m mean -o {output.coverage} -t {threads}
		sed '1d' {output.coverage} > {output.noheader}
		"""

rule metacoag_binning:
	input:
		noheader = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/abundance_metacoag.tsv",
		graph = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/assembly_graph_with_scaffolds.gfa",
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta",
		paths = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.paths"
	output:
		log = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag.log"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/metacoag.yaml"
	shell:
		"""
		metacoag --assembler spades --graph {input.graph} --contigs {input.contigs} --paths {input.paths} --abundance {input.noheader} --output {params.outdir} --nthreads {threads} --min_length 1000 --min_bin_size 50000
		"""

rule metacoag_collect_bins:
	input:
		log = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag.log",
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta"
	output:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag.done"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results",
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag_bins"
	shell:
		"""
		mkdir -p {params.bindir}
		cd {params.bindir}
		ls -1 {params.outdir}/bins/*.fasta | xargs -n 1 basename | while read line
		do
		  cp {params.outdir}/bins/${{line}} ${{line}}
		done
		ls -1 {params.outdir}/low_quality_bins/*.fasta | xargs -n 1 basename | while read line
		do
		  cp {params.outdir}/low_quality_bins/${{line}} ${{line}}
		done
		touch {output.done}
		"""

rule prepare_vamb:
	input:
		contigs = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta", sample = SAMPLES)
	output:
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz"
	conda: config["wdir"] + "/envs/vamb.yaml"
	shell:
		"""
		concatenate.py -m 1000 {output.concat} {input.contigs}
		"""

rule vamb_index:
	input:
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz"
	output:
		index = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz.bwt"
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		bwa index {input.concat}
		"""

# the samtools view command is filtering out supplementary alignments (necessary since mapping was done against contigs from all samples)
# also PCR/optical duplicates are removed and those not passing vendor QC
rule mapping_vamb:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq",
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz",
		index = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz.bwt"
	output:
		bam = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/{sample}.bam"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		bwa mem -v 1 -t {threads} {input.concat} {input.clean_R1} {input.clean_R2} | samtools view -F 3584 -b --threads {threads} > {output.bam}
		"""

# minimum identity of 95% and minimum alignment score of 30
# recommended on https://github.com/RasmussenLab/vamb (date accessed: 2022-03-27)
rule vamb_binning:
	input:
		bam = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/{sample}.bam", sample = SAMPLES),
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/catalogue.fna.gz"
	output:
		log = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/result/log.txt"
	params:
		vambdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/vamb.yaml"
	shell:
		"""
		# vamb --outdir {params.vambdir}/result/tmp --fasta {input.concat} --bamfiles {params.vambdir}/*.bam -o C --minfasta 50000 -z 0.95 -s 30
		vamb --outdir {params.vambdir}/result/tmp --fasta {input.concat} --bamfiles {input.bam} -o C --minfasta 50000 -z 0.95 -s 30
		mv {params.vambdir}/result/tmp/* {params.vambdir}/result/
		"""

def SID_fun(wildcards):
	return VAMB[wildcards.sample]

rule vamb_split_bins:
	input:
		log = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/result/log.txt"
	output:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/vamb_done"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/vamb_binning/result/bins",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/vamb_bins",
		sid = SID_fun
	shell:
		"""
		SID="{params.sid}"
		mkdir -p {params.outdir}
		cd {params.indir}
		ls -1 *.fna | grep "^${{SID}}C" | while read line
		do
		  sed "/^>/s/${{SID}}C//" $line > {params.outdir}/$line
		done
		touch {output.done}
		"""

# Warning: I strongly recommend to NOT set the contamination threshold higher than 10%
# The scoring for selecting the best bin in downstream rules (binspreader and bin reassembly has been optimized for contamination in the range of 0-5 (max 10) 
rule sample_refinement_1:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/bin.unbinned.fa"
	output:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_1/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_1",
		comp = config["completeness"],
		cont = config["contamination"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_1.log"
	shell:
		"""
		metawrap bin_refinement -o {params.outdir} -t {threads} -m 370 -c {params.comp} -x {params.cont} -A {params.indir}/metabat2_bins -B {params.indir}/maxbin2_bins -C {params.indir}/concoct_bins > {log} 2>&1
		"""

rule sample_refinement_2:
	input:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_1/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats",
		done_metacoag = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag.done",
		done_vamb = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/vamb_done"
	output:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats"
	params:
		ref1dir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_1",
		metacoagdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metacoag_binning/results/metacoag_bins",
		vambdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/vamb_bins",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2",
		comp = config["completeness"],
		cont = config["contamination"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2.log"
	shell:
		"""
		mv {params.ref1dir}/metawrap_{params.comp}_{params.cont}_bins {params.ref1dir}/ref1_bins
		metawrap bin_refinement -o {params.outdir} -t {threads} -m 370 -c {params.comp} -x {params.cont} -A {params.ref1dir}/ref1_bins -B {params.metacoagdir} -C {params.vambdir} > {log} 2>&1
		"""

# running binspreader on the refined bin set to retrieve contigs previously not associated with the bin
rule sample_refinement_binspreader:
	input:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats",
		contigs = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.fasta",
		graph = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/assembly_graph_with_scaffolds.gfa",
		paths = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_assembly/{sample}_metaspades/contigs.paths"
	output:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/done"
	params:
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/binspreader.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement.log"
	shell:
		"""
		# prepare contig_bin list
		ls -1 {params.indir}/*.fa | while read line
		do
		  bin=$(echo $line | xargs basename | sed -e 's/\\.fa//')
		  grep '^>' $line | sed -e 's/^>//' -e "s/$/\\t${{bin}}/" 
		done > {params.outdir}/metawrap_refinement_2_results.txt
		# run binspreader
		# mkdir -p {params.outdir}/tmp
		bin-refine {input.graph} {params.outdir}/metawrap_refinement_2_results.txt {params.outdir}/out -t {threads} -Rprop --paths {input.paths} --sparse-propagation --tmp-dir {params.outdir}/tmp > {log} 2>&1
		# remove contigs below 1000kb
		grep -P -v "_length_[0-9]{{0,3}}_" {params.outdir}/out/binning.tsv > {params.outdir}/binspreader_refinement_results.txt
		# parse refined bins
		mkdir -p {params.outdir}/binspreader_bins
		cut -f2 {params.outdir}/binspreader_refinement_results.txt | sort | uniq | while read line
		do
		  grep -w "${{line}}" {params.outdir}/binspreader_refinement_results.txt | cut -f1 | seqkit grep -f - {input.contigs} > {params.outdir}/binspreader_bins/${{line}}.fa
		done
		touch {output.done}
		"""

rule binspreader_checkm:
	input:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/done"
	output:
		stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/binspreader_bins.stats"
	params:
		script_dir = config["metawrap_scripts"],
		tmpdir = config["adir"] + "/tmp",
		checkmdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/binspreader_bins.checkm",
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/binspreader_bins"
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/metawrap.yaml"
	shell:
		"""
		mkdir -p {params.tmpdir}
		checkm lineage_wf -x fa {params.indir} {params.checkmdir} -t {threads} --tmpdir {params.tmpdir} --pplacer_threads 10
		{params.script_dir}/summarize_checkm.py {params.checkmdir}/storage/bin_stats_ext.tsv | (read -r; printf "%s\\n" "$REPLY"; sort) > {output.stats}
		"""

rule binspreader_select_best:
	input:
		ori_stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins.stats",
		aug_stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/binspreader_refinement/binspreader_bins.stats"
	output:
		best_stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/best_bins.stats",
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/done"
	params:
		script = config["wdir"] + "/scripts/select_best_bins.R",
		comp = config["completeness"],
		cont = config["contamination"],
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/metawrap_refinement_2/metawrap_" + str(config["completeness"]) + "_" + str(config["contamination"]) + "_bins",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}"
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		# combine checkm input tables
		echo -e "bin\\tcompleteness\\tcontamination\\tGC\\tlineage\\tN50\\tsize" > {params.outdir}/binspreader_refinement/checkm_stats_parsed.txt
		sed '1d' {input.aug_stats} | sed 's/\\t/_aug\\t/' >> {params.outdir}/binspreader_refinement/checkm_stats_parsed.txt
		sed '1d' {input.ori_stats} | cut -f-7 | sed 's/\\t/_ori\\t/' >> {params.outdir}/binspreader_refinement/checkm_stats_parsed.txt
		# run select best bins
		{params.script} -s {params.outdir}/binspreader_refinement/checkm_stats_parsed.txt -o {output.best_stats} -a 5 -b 1.2 -c {params.comp} -r {params.cont}
		# extract fa depending from respective bin sets
		mkdir -p {params.outdir}/best_bins
		if cut -f1 {output.best_stats} | sed '1d' | grep -q "_aug$"
		then
		  cut -f1 {output.best_stats} | sed '1d' | grep "_aug$" | sed 's/_aug$//' | while read line
		  do
		    cp {params.outdir}/binspreader_refinement/binspreader_bins/${{line}}.fa {params.outdir}/best_bins/${{line}}_aug.fa
		  done
		fi
		cut -f1 {output.best_stats} | sed '1d' | grep "_ori$" | sed 's/_ori$//' | while read line
		do
		  cp {params.bindir}/${{line}}.fa {params.outdir}/best_bins/${{line}}_ori.fa
		done
		touch {output.done}
		"""

rule quant_bins_sample:
	input:
		unbinned_concoct = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/unbinned.fa",
		unbinned_metabat2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/bin.unbinned.fa",
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/best_bins.stats"
	output:
		sample_quant = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/{sample}_bins_quant.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_binning/{sample}/metawrap_binning/work_files",
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Single_refinement/{sample}/best_bins",
		pident = config["read_identity"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		coverm genome -b {params.bamdir}/*.bam -m mean --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -d {params.bindir} -x fa -t {threads} > {output.sample_quant}
		"""


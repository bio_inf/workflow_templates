rule estimate_coverage:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq"
	output:
		nonpareil = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/{sample}_nonpareil.npo"
	params:
		filebase = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/{sample}_nonpareil"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/nonpareil.yaml"
	shell:
		"""
		nonpareil -s {input.clean_R1} -T kmer -f fastq -b {params.filebase} -R 100000 -t {threads}
		"""

rule parse_nonpareil:
	input:
		nonpareil = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/{sample}_nonpareil.npo", sample = SAMPLES)
	output:
		plot = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/nonpareil_curves_" + config["project"] + ".pdf",
		txt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/nonpareil_summary_" + config["project"] + ".txt"
	params:
		script = config["wdir"] + "/scripts/nonpareil_parse.R",
		indir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats"
	conda: config["wdir"] + "/envs/nonpareil.yaml"
	shell:
		"""
		{params.script} -d {params.indir} -p {output.plot} -s {output.txt}
		"""

rule extract_ssu:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		ssu_fasta = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/{sample}_phyloflash_out/{sample}.all.final.fasta",
		vsearch_csv = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/{sample}_phyloflash_out/{sample}.all.vsearch.csv",
		vsearch_final = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/{sample}_phyloflash_out/{sample}_all_vsearch_final.csv"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/{sample}_phyloflash_out",
		sample = "{sample}",
		read_length = config["read_length"],
		db = config["phyloflash"]
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/phyloflash.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/{sample}_phyloflash.log"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		phyloFlash.pl -lib {params.sample} -read1 {input.clean_R1} -read2 {input.clean_R2} -readlength {params.read_length} -dbhome {params.db} -CPUs {threads} -emirge > {log} 2>&1
		echo -e "query\\ttarget\\tpident\\talnlen\\tevalue\\tid3_mbl\\tqlen\\tpairs\\tgaps\\tmismatches\\tmatches" > {output.vsearch_final}
		cat {output.vsearch_csv} >> {output.vsearch_final}
		"""

rule classify_kraken2:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		kraken = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.kraken",
		kreport = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.kreport"
	params:
		dbdir = config["kraken2_dbdir"],
		conf = config["confidence"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/kraken2.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.log"
	shell:
		"""
		kraken2 --db {params.dbdir} --confidence {params.conf} --threads {threads} --report {output.kreport} --output {output.kraken} --report-minimizer-data --paired {input.clean_R1} {input.clean_R2} &>> {log}
		"""

rule calculate_scores_kraken2:
	input:
		kraken = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.kraken"
	output:
		conifer_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.conifer"
	params:
		taxo = config["kraken2_dbdir"] + "/taxo.k2d"
	conda: config["wdir"] + "/envs/kraken2.yaml"
	shell:
		"""
		conifer --all --both_scores -i {input.kraken} -d {params.taxo} > {output.conifer_out}
		"""

rule retrieve_taxid_kraken2:
	input:
		conifer_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.conifer"
	output:
		kraken2_tax = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.tax"
	params:
		taxdir = config["taxdb_taxdir"]
	conda: config["wdir"] + "/envs/taxonkit.yaml"
	shell:
		"""
		cut -f3 {input.conifer_out} | taxonkit lineage --data-dir {params.taxdir} | cut -f2 | paste {input.conifer_out} - > {output.kraken2_tax}
		"""

rule summarize_kraken2:
	input:
		kraken2_tax = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kraken_out/{sample}.tax", sample = SAMPLES)
	output:
		tax_summary = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/summary_kraken2.txt"
	params:
		script = config["wdir"] + "/scripts/summarize_kraken2.R",
		rtl = config["rtl"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -i "{input.kraken2_tax}" -o {output.tax_summary} -r {params.rtl} -c 0 -t {threads}
		"""

# for now, the settings for allowed mismatches (-e 5) and minimum e-value (-E 0.1) are hard-coded in the rule
# the mismatch setting is slightly more relaxed than the default (3)
rule classify_kaiju:
	input:
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		kaiju_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kaiju_out/{sample}_kaiju.out"
	params:
		nodes = config["taxdb_taxdir"] + "/nodes.dmp",
		kaijudb = config["kaiju_fmi"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/kaiju.yaml"
	shell:
		"""
		kaiju -e 5 -E 0.1 -v -z {threads} -t {params.nodes} -f {params.kaijudb} -i {input.clean_R1} -j {input.clean_R2} -o {output.kaiju_out}
		"""

rule retrieve_taxid_kaiju:
	input:
		kaiju_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kaiju_out/{sample}_kaiju.out"
	output:
		kaiju_tax = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kaiju_out/{sample}_kaiju.tax"
	params:
		taxdir = config["taxdb_taxdir"]
	conda: config["wdir"] + "/envs/taxonkit.yaml"
	shell:
		"""
		cut -f3 {input.kaiju_out} | taxonkit lineage --data-dir {params.taxdir} > {output.kaiju_tax}
		"""

rule summarize_kaiju:
	input:
		kaiju_tax = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/kaiju_out/{sample}_kaiju.tax", sample = SAMPLES)
	output:
		tax_summary = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Taxonomy/summary_kaiju.txt"
	params:
		script = config["wdir"] + "/scripts/summarize_kaiju.R"
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -i "{input.kaiju_tax}" -o {output.tax_summary}
		"""


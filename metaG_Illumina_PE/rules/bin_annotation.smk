rule final_bins_index:
	input:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/done"
	output:
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/all_contigs.fa",
		index = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/all_contigs.fa.bwt"
	params:
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/bins"
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		cat {params.bindir}/*.fa > {output.concat}
		bwa index {output.concat}
		"""

rule final_bins_mapping:
	input:
		concat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/all_contigs.fa",
		clean_R1 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_1.fastq",
		clean_R2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Human_cleaned/final/{sample}_2.fastq"
	output:
		bam = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/{sample}.bam"
	params:
		sample = "{sample}",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification"
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		bwa mem -v 1 -t {threads} {input.concat} {input.clean_R1} {input.clean_R2} | samtools sort -T {params.outdir}/tmp_{params.sample}_samtools -@ {threads} -O BAM > {output.bam}
		"""

rule final_bins_mapstats:
	input:
		bam = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/{sample}.bam"
	output:
		mapstats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/{sample}_mapstats.txt"
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		samtools flagstat {input.bam} > {output.mapstats}
		"""

rule final_bins_coverage:
	input:
		bam = expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/{sample}.bam", sample = SAMPLES)
	output:
		rel = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/bin_rel_abund_coverm.txt",
		mean = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/bin_mean_cov_coverm.txt"
	params:
		bamdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification",
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/bins",
		pident = config["read_identity"]
	threads: config["sample_threads"]
	conda: config["wdir"] + "/envs/coverm.yaml"
	shell:
		"""
		coverm genome -b {params.bamdir}/*.bam -m relative_abundance --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -d {params.bindir} -x fa -t {threads} > {output.rel}
		coverm genome -b {params.bamdir}/*.bam -m mean --min-read-percent-identity {params.pident} --min-read-aligned-length 50 --exclude-supplementary -d {params.bindir} -x fa -t {threads} > {output.mean}
		"""

rule final_bins_gtdbtk:
	input:
		done_bins = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/done"
	output:
		tax_ar = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.ar53.summary.tsv",
		tax_bac = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.bac120.summary.tsv"
	params:
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/bins",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out",
		mashdb = config["gtdb_mash"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/gtdbtk.yaml"
	shell:
		"""
		gtdbtk classify_wf --genome_dir {params.bindir} --out_dir {params.outdir} -x fa --min_af 0.5 --cpus {threads} --mash_db {params.mashdb}
		# if no archaeal bins are present, generate an empty file with just the header row
		if [[ ! -f {output.tax_ar} ]] && tail -1 {params.outdir}/gtdbtk.log | grep -q "Done"
		then
		  echo -e 'user_genome\\tclassification\\tfastani_reference\\tfastani_reference_radius\\tfastani_taxonomy\\tfastani_ani\\tfastani_af\\tclosest_placement_reference\\tclosest_placement_radius\\tclosest_placement_taxonomy\\tclosest_placement_ani\\tclosest_placement_af\\tpplacer_taxonomy\\tclassification_method\\tnote\\tother_related_references(genome_id,species_name,radius,ANI,AF)\\tmsa_percent\\ttranslation_table\\tred_value\\twarnings' > {output.tax_ar}
		fi
		"""

rule final_bins_bat:
	input:
		done = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/done",
		checkm2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/checkm2_out/quality_report.tsv"
	output:
		classified = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/bat_out/gtdb.BAT.bin2classification.txt",
		names = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/bat_out/gtdb.BAT.bin2classification.names.txt"
	params:
		bindir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/bins",
		faadir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/checkm2_out/protein_files",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/bat_out",
		dbdir = config["cat_gtdb_db"],
		taxdir = config["cat_gtdb_tax"],
		bat_r = config["bat_r"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/cat.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		cd {params.outdir}
		ls -1 {params.faadir}/*.faa | xargs -n 1 basename | sed 's/a$//' | while read line
		do
		  sed "/^>/s/^>/>${{line}}_/" {params.faadir}/${{line}}a
		done > gtdb.BAT.concatenated.predicted_proteins.faa
		CAT bins -b {params.bindir} -d {params.dbdir} -t {params.taxdir} -s fa -o gtdb.BAT -p gtdb.BAT.concatenated.predicted_proteins.faa -n {threads} -r {params.bat_r} --I_know_what_Im_doing --top 11
		CAT add_names -i {output.classified} -o {output.names} -t {params.taxdir} --exclude_scores
		"""

# consider reducing min ORF length for more sensitive annotation (at the moment only proteins with more than 60aa are considered)
rule metaerg_annotation:
	input:
		fasta = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/bins/{bin}.fa",
		tax_ar = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.ar53.summary.tsv",
		tax_bac = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.bac120.summary.tsv"
	output:
		html = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/index.html",
		rrna = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/rRNA.tab.txt",
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/cds.faa",
		gff = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/all.gff",
		master = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/master.tsv.txt"
	params:
		bin = "{bin}",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/metaerg.yaml"
	log: config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/logs/{bin}_metaerg.log"
	shell:
		"""
		if grep -q "{params.bin}" {input.tax_ar}
		then
		  metaerg.pl --gtype arc --sp --tm --minorflen 180 --prefix {params.bin} --outdir {params.outdir}/metaerg_tmp --cpus {threads} --force {input.fasta} >> {log} 2>&1
		fi
		if grep -q "{params.bin}" {input.tax_bac}
		then
		  metaerg.pl --gtype bac --sp --tm --minorflen 180 --prefix {params.bin} --outdir {params.outdir}/metaerg_tmp --cpus {threads} --force {input.fasta} >> {log} 2>&1
		fi
		mv {params.outdir}/metaerg_tmp/* {params.outdir}/
		cd {params.outdir}
		tar -czf tmp.tar.gz tmp/
		rmdir metaerg_tmp
		rm -rf tmp/ 
		"""

rule kegg_metaerg:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/cds.faa"
	output:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_kegg_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_self_diamond.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}",
		kegg_dmnd = config["kegg_diamond_db"],
		evalue = config["evalue"]
	threads: config["parallel_threads"]
	conda: config["wdir"] + "/envs/dbcan.yaml"
	shell:
		"""
		diamond blastp -d {params.kegg_dmnd} --sensitive -e {params.evalue} -q {input.cds} --top 10 -p {threads} -o {output.blast_kegg} -b 8 -f 6
		diamond makedb --in {input.cds} -d {params.outdir}/cds_db
		diamond blastp -d {params.outdir}/cds_db.dmnd -q {input.cds} -k 1 -o {output.blast_self} -f 6
		rm {params.outdir}/cds_db.dmnd
		"""

rule parse_kegg:
	input:
		blast_kegg = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_kegg_diamond.txt",
		blast_self = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_self_diamond.txt"
	output:
		kegg_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_kegg_parsed.txt",
		kegg_best = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}/out_kegg_parsed_best.txt"
	params:
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kegg_out/{bin}",
		script = config["wdir"] + "/scripts/parse_kegg.R",
		mapdir = config["kegg_mapdir"],
		bsr = config["bsr"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -b {input.blast_kegg} -s {input.blast_self} -m {params.mapdir} -c {params.bsr} -t 1 -o {params.outdir}/out_kegg_parsed
		"""

# the e-value setting was taken from https://www.genome.jp/tools/kofamkoala/ (date accessed: 2022-03-27)
rule kofam_metaerg:
	input:
		cds = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/cds.faa"
	output:
		kofam_out = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kofam_out/{bin}/out_kofamscan.txt",
		kofam_parsed = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kofam_out/{bin}/out_kofam_parsed.txt"
	params:
		kofam_db = config["kofam_db"],
		tmpdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kofam_out/tmp_{bin}",
		outdir = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/kofam_out/{bin}"
	threads: config["annotation_threads"]
	conda: config["wdir"] + "/envs/kofamscan.yaml"
	shell:
		"""
		mkdir -p {params.outdir}
		exec_annotation -o {output.kofam_out} -p {params.kofam_db}/profiles -k {params.kofam_db}/ko_list --tmp-dir={params.tmpdir} --cpu {threads} -E 0.01 {input.cds}
		grep "^\\*" {output.kofam_out} | sed 's/^\\* //' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sed -E 's/ +/\\t/' | sort -k1,1 -k4,4gr -k5,5g | sort -u -k1,1 --merge > {output.kofam_parsed}
		rm -rf {params.tmpdir}
		"""

def aggregate_metaerg(wildcards):
	checkpoint_output = checkpoints.collect_final_bins.get().output['outdir']
	fasta_files = sorted(glob.glob(os.path.join(checkpoint_output, '*.fa')))
	bin = [re.sub('\.fa$', '', os.path.basename(x)) for x in fasta_files]
	return expand(config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_out/{bin}/data/rRNA.tab.txt", bin = bin)

rule summarize_ssu:
	input:
		metaerg_rrna = aggregate_metaerg
	output:
		metaerg_ssu = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_summary_16S.txt"
	shell:
		"""
		grep "16SrRNA" {input.metaerg_rrna} | sed -e 's/:/\\t/' -e 's/.*\\/\\([^/]*\\)\\/data\\/rRNA\\.tab\\.txt/\\1/' > {output.metaerg_ssu}
		"""

rule parse_stats_tax:
	input:
		checkm = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_reassembly/best_bins.stats",
		checkm2 = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/checkm2_out/quality_report.tsv",
		gunc = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_final/gunc_out/gunc_out_all_levels.tsv",
		rel = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_quantification/bin_rel_abund_coverm.txt",
		tax_ar = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.ar53.summary.tsv",
		tax_bac = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/gtdbtk_out/gtdbtk.bac120.summary.tsv",
		tax_bat = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_classification/bat_out/gtdb.BAT.bin2classification.names.txt",
		metaerg_ssu = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Bin_annotation/metaerg_summary_16S.txt"
	output:
		plot = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/final_bins_summary.pdf",
		stats = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Summary_stats/final_bins_summary.txt"
	params:
		script = config["wdir"] + "/scripts/final_bins.R",
		basedir = config["adir"] + "/Intermediate_results/" + config["analysis"]
	conda: config["wdir"] + "/envs/r.yaml"
	shell:
		"""
		{params.script} -d {params.basedir}
		"""

#!/bin/bash

SEQ=$1
BLAST=$2
RSCRIPT=$3
ACC2TAX=$4
MAP=$5
DIR=$6
LIN=$7

SEQHASH=$(echo "${SEQ}" | sha1sum | head -c 40)

echo $SEQ | grep -w -F -f - $BLAST > $DIR/${SEQHASH}.txt

$RSCRIPT -i $DIR/${SEQHASH}.txt -o $DIR/${SEQHASH}_out.txt -m $ACC2TAX -r $MAP -l $LIN

cat $DIR/${SEQHASH}_out.txt

rm $DIR/${SEQHASH}.txt $DIR/${SEQHASH}_out.txt




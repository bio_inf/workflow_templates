#!/usr/bin/env Rscript

# parse assembly metadata for NCBI genomes

### setting up environment ####

# Check if packages are installed
package.list <- c(
  "crayon",
  "optparse",
  "config",
  "dada2",
  "ShortRead",
  "ggplot2",
  "gridExtra",
  "Biostrings",
  "scales",
  "furrr",
  "tidyverse"
)

# Function to check if packages are installed
is.installed <- function(pkg){
  is.element(pkg, installed.packages()[,1])
}

# If not all packages are available
if(any(!is.installed(package.list))) {
  cat("Not all required packages are available. They will now be installed.\n")
  
  # give the user the chance to abort manually
  Sys.sleep(20)
  
  # then install packages
  for(i in which(!is.installed(package.list))) {
    suppressMessages(install.packages(package.list[i], repos = "http://cran.us.r-project.org"))
  }
}

# Break the script if the package installation was unsuccessful
if(any(!is.installed(package.list))) {
  cat(
    paste0(
      "Unable to install all required packages.\nPlease install ",
      paste0(package.list[!is.installed(package.list)], collapse = ", "),
      " manually."
    )
  )
  break
}

# Load packages
cat("Loading libraries...")
silent <- suppressMessages(lapply(package.list, function(X) {require(X, character.only = TRUE)}))
rm(silent)
cat(" done\n")

# Some functions for message output
msg <- function(X){
  cat(crayon::white(paste0("[",format(Sys.time(), "%T"), "]")), X)
}
msg_sub <- function(X){
  cat(crayon::white(paste0("  [",format(Sys.time(), "%T"), "]")), X)
}


### Reading command line options ####

# define command line options
option_list <- list(
  make_option(
    c("-n", "--name"),
    type = "character",
    default = NULL,
    help = "project name (will serve as file base name for output files",
    metavar = "character"
  ),
  make_option(
    c("-s", "--samples"),
    type = "character",
    default = NULL,
    help = "file name of sample list (has to be in first column, no headers)",
    metavar = "character"
  ),
  make_option(
    c("-l", "--truncLen_R1"),
    type = "integer",
    default = NULL,
    help = "truncation length for R1 read",
    metavar = "number"
  ),
  make_option(
    c("-L", "--truncLen_R2"),
    type = "integer",
    default = NULL,
    help = "truncation length for R2 read",
    metavar = "number"
  ),
  make_option(
    c("-f", "--frag_min"),
    type = "integer",
    default = NULL,
    help = "minimum size of expected fragment",
    metavar = "number"
  ),
  make_option(
    c("-F", "--frag_max"),
    type = "integer",
    default = NULL,
    help = "maximum size of expected fragment",
    metavar = "number"
  ),
  make_option(
    c("-e", "--error_R1"),
    type = "integer",
    default = NULL,
    help = "maximum expected error rate R1 read",
    metavar = "number"
  ),
  make_option(
    c("-E", "--error_R2"),
    type = "integer",
    default = NULL,
    help = "maximum expected error rate R2 read",
    metavar = "number"
  ),
  make_option(
    c("-m", "--loess"),
    type = "character",
    default = NULL,
    help = "Should the modified loess error function be used (set to mod)",
    metavar = "character"
  ),
  make_option(
    c("-P", "--pool"),
    type = "character",
    default = "TRUE",
    help = "Pooling option for the dada function (default: TRUE)",
    metavar = "character"
  ),
  make_option(
    c("-r", "--rescue"),
    type = "character",
    default = NULL,
    help = "If location of mapping reference is given here, unmerged reads will be attempted to be rescued",
    metavar = "character"
  ),
  make_option(
    c("-t", "--prior_seqtab"),
    type = "character",
    default = "none",
    help = "Absolute path to RDS file of sequence table from previous analysis",
    metavar = "character"
  ),
  make_option(
    c("-d", "--dir"),
    type = "character",
    default = NULL,
    help = "path to analysis directory (highest level directory for sequencing project",
    metavar = "character"
  ),
  make_option(
    c("-a", "--analysis"),
    type = "character",
    default = NULL,
    help = "name of the analysis (version) that is being run",
    metavar = "character"
  ),
  make_option(
    c("-x", "--prefix"),
    type = "character",
    default = NULL,
    help = "prefix to use for sequence header",
    metavar = "character"
  ),
  make_option(
    c("-o", "--orientation"),
    type = "character",
    default = "fr",
    help = "specify if libraries were generated in mixed orientation",
    metavar = "character"
  ),
  make_option(
    c("-c", "--cpus"),
    type = "integer",
    default = 1,
    help = "number of threads [default: 1]",
    metavar = "number"
  )
)

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)

if (is.null(opt$name) | is.null(opt$samples) | is.null(opt$truncLen_R1) |
    is.null(opt$truncLen_R2) | is.null(opt$frag_min) | is.null(opt$frag_max) |
    is.null(opt$error_R1) | is.null(opt$error_R2) | is.null(opt$dir) | is.null(opt$analysis)) {
  print_help(opt_parser)
  stop("All parameters are mandatory.\n", call. = FALSE)
}


### Defining functions ####

# Wrapper function to create multi-page pdf with base quality plots
# Argument 'file_base' will be extended by '_R1.pdf' and '_R2.pdf'
quality_check <- function(
  R1, 
  R2, 
  file_base = "QualityProfile",
  cpus = 1
) {
  
  # start parallel session
  plan(multicore, workers = cpus)
  
  # R1 profiles
  QualityProfileFs <- future_map(
    1:length(R1),
    function(x) {
      tmp <- list()
      tmp[[1]] <- plotQualityProfile(R1[x])
      return(tmp)
    }
  )
  pdf(paste0(file_base, "_R1.pdf"))
  for(i in 1:length(R1)) {
    do.call("grid.arrange", QualityProfileFs[[i]])  
  }
  dev.off()
  
  # R2 profiles
  QualityProfileRs <- future_map(
    1:length(R2),
    function(x) {
      tmp <- list()
      tmp[[1]] <- plotQualityProfile(R2[x])
      return(tmp)
    }
  )
  pdf(paste0(file_base, "_R2.pdf"))
  for(i in 1:length(R2)) {
    do.call("grid.arrange", QualityProfileRs[[i]])  
  }
  dev.off()
}

# in case the data were produced with binned quality scores:
# https://github.com/benjjneb/dada2/issues/1307
loessErrfun2 <- function (trans) {
  qq <- as.numeric(colnames(trans))
  est <- matrix(0, nrow = 0, ncol = length(qq))
  for (nti in c("A", "C", "G", "T")) {
    for (ntj in c("A", "C", "G", "T")) {
      if (nti != ntj) {
        errs <- trans[paste0(nti, "2", ntj), ]
        tot <- colSums(trans[paste0(nti, "2", c("A",
                                                "C", "G", "T")), ])
        rlogp <- log10((errs + 1)/tot)
        rlogp[is.infinite(rlogp)] <- NA
        df <- data.frame(q = qq, errs = errs, tot = tot,
                         rlogp = rlogp)
        # Gulliem Salazar's solution
        # https://github.com/benjjneb/dada2/issues/938
        mod.lo <- loess(rlogp ~ q, df, weights = log10(tot), span = 2)
        pred <- predict(mod.lo, qq)
        maxrli <- max(which(!is.na(pred)))
        minrli <- min(which(!is.na(pred)))
        pred[seq_along(pred) > maxrli] <- pred[[maxrli]]
        pred[seq_along(pred) < minrli] <- pred[[minrli]]
        est <- rbind(est, 10^pred)
      }
    }
  }
  MAX_ERROR_RATE <- 0.25
  MIN_ERROR_RATE <- 1e-07
  est[est > MAX_ERROR_RATE] <- MAX_ERROR_RATE
  est[est < MIN_ERROR_RATE] <- MIN_ERROR_RATE
  err <- rbind(1 - colSums(est[1:3, ]), est[1:3, ], est[4,
  ], 1 - colSums(est[4:6, ]), est[5:6, ], est[7:8, ], 1 -
    colSums(est[7:9, ]), est[9, ], est[10:12, ], 1 - colSums(est[10:12,
    ]))
  rownames(err) <- paste0(rep(c("A", "C", "G", "T"), each = 4),
                          "2", c("A", "C", "G", "T"))
  colnames(err) <- colnames(trans)
  return(err)
}

# extract unmerged reads
# input objects are expected to be lists with data of several samples
extract_unmerged <- function(dadaF, dadaR, mergers) {
  
  # R1 read
  unmergedFs <- lapply(
    1:length(mergers),
    function(x) {
      if(sum(!mergers[[x]]$accept) > 0) {
	tmp <- dadaF[[x]]$sequence[mergers[[x]]$forward[!mergers[[x]]$accept]]
        names(tmp) <- paste0(x, "_", which(!mergers[[x]]$accept), "/1")
        return(tmp)
      }
    }
  )
  
  # R2 read
  unmergedRs <- lapply(
    1:length(mergers),
    function(x) {
      if(sum(!mergers[[x]]$accept) > 0) {
	tmp <- dadaR[[x]]$sequence[mergers[[x]]$reverse[!mergers[[x]]$accept]]
        names(tmp) <- paste0(x, "_", which(!mergers[[x]]$accept), "/2")
        return(tmp)
      }
    }
  )
  
  # create one fasta with unmerged from all samples
  fastaFs <- unlist(unmergedFs)
  fastaRs <- unlist(unmergedRs)
  
  # output is a list with paired R1 and R2
  return(list(fastaFs, fastaRs))
  
}


### Defining input and output file paths ####

msg(paste0("This is dada2 version: ", packageVersion("dada2"), ".\n"))

# set sample names
sample.names <- read.table(opt$samples, h = F, sep = "\t", stringsAsFactors = F)[, 1]

# Specify path to input sequences
path <- paste0(opt$dir, "/Validated_data/", opt$analysis)
fnFs.fr <- paste0(path, "/", sample.names, "_clip_fr_R1.fastq.gz")
fnRs.fr <- paste0(path, "/", sample.names, "_clip_fr_R2.fastq.gz")
names(fnFs.fr) <- sample.names
names(fnRs.fr) <- sample.names

if(opt$orientation == "mixed") {
  fnFs.rf <- paste0(path, "/", sample.names, "_clip_rf_R1.fastq.gz")
  fnRs.rf <- paste0(path, "/", sample.names, "_clip_rf_R2.fastq.gz")
  names(fnFs.rf) <- sample.names
  names(fnRs.rf) <- sample.names
}

# Place filtered files in Filtered/ subdirectory
filtFs.fr <- file.path(opt$dir, "Intermediate_results", opt$analysis, "Filtered", paste0(sample.names, "_filt_fr_R1.fastq.gz"))
filtRs.fr <- file.path(opt$dir, "Intermediate_results", opt$analysis, "Filtered", paste0(sample.names, "_filt_fr_R2.fastq.gz"))
names(filtFs.fr) <- sample.names
names(filtRs.fr) <- sample.names

if(opt$orientation == "mixed") {
  filtFs.rf <- file.path(opt$dir, "Intermediate_results", opt$analysis, "Filtered", paste0(sample.names, "_filt_rf_R1.fastq.gz"))
  filtRs.rf <- file.path(opt$dir, "Intermediate_results", opt$analysis, "Filtered", paste0(sample.names, "_filt_rf_R2.fastq.gz"))
  names(filtFs.rf) <- sample.names
  names(filtRs.rf) <- sample.names
}


### Filter and trim reads with optimized parameters ####

# Run trimming with optimal parameters
filt.out.fr <- filterAndTrim(
  fwd = fnFs.fr, 
  filt = filtFs.fr, 
  rev = fnRs.fr, 
  filt.rev = filtRs.fr,
  truncLen = c(opt$truncLen_R1, opt$truncLen_R2),
  maxN = 0,
  minQ = 2,
  maxEE = c(opt$error_R1, opt$error_R2), 
  truncQ = 0, 
  rm.phix = TRUE,
  compress = TRUE,
  multithread = opt$cpus
)
if(opt$orientation == "mixed") {
  filt.out.rf <- filterAndTrim(
    fwd = fnFs.rf, 
    filt = filtFs.rf, 
    rev = fnRs.rf, 
    filt.rev = filtRs.rf,
    truncLen = c(opt$truncLen_R1, opt$truncLen_R2),
    maxN = 0,
    minQ = 2,
    maxEE = c(opt$error_R1, opt$error_R2), 
    truncQ = 0, 
    rm.phix = TRUE,
    compress = TRUE,
    multithread = opt$cpus
 )
}
msg("Summary of retained reads after quality filtering:\n")
if(opt$orientation == "mixed") {
  summary(filt.out.fr[, 2] + filt.out.rf[, 2])
} else {
  summary(filt.out.fr[, 2])
}
msg("And in percentages:\n")
if(opt$orientation == "mixed") {
  summary((filt.out.fr[, 2] + filt.out.rf[, 2])/(filt.out.fr[, 1] + filt.out.rf[, 1]))
} else {
  summary(filt.out.fr[, 2]/filt.out.fr[, 1])
}

# Repeat quality check after trimming
if(opt$orientation == "mixed") {
  quality_check(
    c(filtFs.fr, filtFs.rf),
    c(filtRs.fr, filtRs.rf),
    file_base = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/QualityProfileFiltered_", opt$name),
    cpus = opt$cpus
  )
} else {
  quality_check(
    filtFs.fr,
    filtRs.fr,
    file_base = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/QualityProfileFiltered_", opt$name),
    cpus = opt$cpus
  )
}


### Denoising ####

# Learn error rates
if(opt$loess == "mod") {
  errF.fr <- learnErrors(filtFs.fr, errorEstimationFunction = loessErrfun2, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  errR.fr <- learnErrors(filtRs.fr, errorEstimationFunction = loessErrfun2, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  if(opt$orientation == "mixed") {
    errF.rf <- learnErrors(filtFs.rf, errorEstimationFunction = loessErrfun2, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
    errR.rf <- learnErrors(filtRs.rf, errorEstimationFunction = loessErrfun2, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  }
} else {
  errF.fr <- learnErrors(filtFs.fr, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  errR.fr <- learnErrors(filtRs.fr, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  if(opt$orientation == "mixed") {
    errF.rf <- learnErrors(filtFs.rf, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
    errR.rf <- learnErrors(filtRs.rf, multithread = opt$cpus, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
  }
}
save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))

# check convergence of error estimation and plot error profiles
pdf(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/ErrorProfiles_", opt$name, ".pdf"))
barplot(log10(dada2:::checkConvergence(errF.fr) + 1), main = "Convergence_fwd")
barplot(log10(dada2:::checkConvergence(errR.fr) + 1), main = "Convergence_rev")
if(opt$orientation == "mixed") {
  barplot(log10(dada2:::checkConvergence(errF.rf) + 1), main = "Convergence_fwd")
  barplot(log10(dada2:::checkConvergence(errR.rf) + 1), main = "Convergence_rev")
}
plotErrors(errF.fr, nominalQ = TRUE)
plotErrors(errR.fr, nominalQ = TRUE)
if(opt$orientation == "mixed") {
  plotErrors(errF.rf, nominalQ = TRUE)
}
if(opt$orientation == "mixed") {
  plotErrors(errR.rf, nominalQ = TRUE)
}
dev.off()

# Dereplicate and denoise samples
if(opt$pool == "pseudo") {
  dadaFs.fr <- dada(filtFs.fr, err = errF.fr, multithread = opt$cpus, pool = opt$pool)
  dadaRs.fr <- dada(filtRs.fr, err = errR.fr, multithread = opt$cpus, pool = opt$pool)
  if(opt$orientation == "mixed") {
    dadaFs.rf <- dada(filtFs.rf, err = errF.rf, multithread = opt$cpus, pool = opt$pool)
    dadaRs.rf <- dada(filtRs.rf, err = errR.rf, multithread = opt$cpus, pool = opt$pool)
  }
} else {
  dadaFs.fr <- dada(filtFs.fr, err = errF.fr, multithread = opt$cpus, pool = as.logical(opt$pool))
  dadaRs.fr <- dada(filtRs.fr, err = errR.fr, multithread = opt$cpus, pool = as.logical(opt$pool))
  if(opt$orientation == "mixed") {
    dadaFs.rf <- dada(filtFs.rf, err = errF.rf, multithread = opt$cpus, pool = as.logical(opt$pool))
    dadaRs.rf <- dada(filtRs.rf, err = errR.rf, multithread = opt$cpus, pool = as.logical(opt$pool))
  }
}
save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))


### Merging ####

# Merge reads
mergers.fr0 <- mergePairs(
  dadaFs.fr,
  filtFs.fr, 
  dadaRs.fr, 
  filtRs.fr, 
  minOverlap = 10,
  verbose = TRUE,
  returnRejects = TRUE
)
if(opt$orientation == "mixed") {
  mergers.rf0 <- mergePairs(
    dadaFs.rf,
    filtFs.rf, 
    dadaRs.rf, 
    filtRs.rf, 
    minOverlap = 10,
    verbose = TRUE,
    returnRejects = TRUE
  )
}

if(length(sample.names) == 1) {
  mergers.fr0 <- list(mergers.fr0)
  names(mergers.fr0) <- sample.names
  if(opt$orientation == "mixed") {
    mergers.rf0 <- list(mergers.rf0)
    names(mergers.rf0) <- sample.names
  }
}

save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))

# rescue unmerged
if(!is.null(opt$rescue)) {
  
  # write unmerged to file
  unmerged.fr <- extract_unmerged(dadaFs.fr, dadaRs.fr, mergers.fr0)
  writeFasta(unmerged.fr[[1]], file = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Fs.fr.fasta"))
  writeFasta(unmerged.fr[[2]], file = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Rs.fr.fasta"))
  
  if(opt$orientation == "mixed") {
    unmerged.rf <- extract_unmerged(dadaFs.rf, dadaRs.rf, mergers.rf0)
    writeFasta(unmerged.rf[[1]], file = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Fs.rf.fasta"))
    writeFasta(unmerged.rf[[2]], file = paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Rs.rf.fasta"))
  }

  # mapping
  system(
    paste0(
      "bbmap.sh threads=",
      opt$cpus,
      " ref=",
      opt$rescue, 
      " in=",
      opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Fs.fr.fasta",
      " in2=",
      opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Rs.fr.fasta",
      " out=",
      opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_unmerged.fr.bam"
    )
  )
  if(opt$orientation == "mixed") {
    system(
      paste0(
        "bbmap.sh threads=",
        opt$cpus,
        " ref=",
        opt$rescue, 
        " in=",
        opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Fs.rf.fasta",
        " in2=",
        opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_Rs.rf.fasta",
        " out=",
        opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_unmerged.rf.bam"
      )
    )
  }

  # extract insert size
  system(
    paste0(
      "samtools view -F2304 -f66 -m50 ",
      opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_unmerged.fr.bam",
      " | cut -f1,9 > ",
      opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_is.fr.txt"
    )
  )
  if(opt$orientation == "mixed") {
    system(
      paste0(
        "samtools view -F2304 -f66 -m50 ",
        opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_unmerged.rf.bam",
        " | cut -f1,9 > ",
        opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_is.rf.txt"
      )
    )
  }

  # read insert sizes
  is.fr <- read.table(
    paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_is.fr.txt"),
    h = F, 
    sep = "\t",
    col.names = c("seqID", "insert")
  )
  if(opt$orientation == "mixed") {
    is.rf <- read.table(
      paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/unmerged_", opt$name, "_is.rf.txt"),
      h = F, 
      sep = "\t",
      col.names = c("seqID", "insert")
    )
  }

  # filter to insert sizes that exceed maximum length of merged sequences
  is_long.fr <- is.fr[is.fr$insert > (opt$truncLen_R1 + opt$truncLen_R2 - 10), ] %>% 
    separate(seqID, into = c("sample_index", "row_index"), sep = "_", remove = F, convert = T)
  if(opt$orientation == "mixed") {
    is_long.rf <- is.rf[-is.rf$insert > (opt$truncLen_R1 + opt$truncLen_R2 - 10), ] %>% 
      separate(seqID, into = c("sample_index", "row_index"), sep = "_", remove = F, convert = T)
  }

  # retrieve and concatenate sequence
  mergers.fr <- mergers.fr0
  for(i in 1:length(mergers.fr)) {
    if(i %in% unique(is_long.fr$sample_index)) {
      tmp_index <- is_long.fr$row_index[is_long.fr$sample_index == i]
      if(length(tmp_index) > 0) {
        mergers.fr[[i]]$sequence[tmp_index] <- paste0(
          unmerged.fr[[1]][paste0(is_long.fr$seqID[is_long.fr$sample_index == i], "/1")],
          "NNNNNNNNNN", 
          rc(unmerged.fr[[2]][paste0(is_long.fr$seqID[is_long.fr$sample_index == i], "/2")])
        )
        mergers.fr[[i]]$nmatch[tmp_index] <- 0
        mergers.fr[[i]]$nmismatch[tmp_index] <- 0
        mergers.fr[[i]]$nindel[tmp_index] <- 0
        mergers.fr[[i]]$prefer[tmp_index] <- NA
        mergers.fr[[i]]$accept[tmp_index] <- TRUE
        mergers.fr[[i]] <- mergers.fr[[i]][mergers.fr[[i]]$accept, ]
      }
    } else {
      mergers.fr[[i]] <- mergers.fr[[i]][mergers.fr[[i]]$accept, ]
    }
  }
  if(opt$orientation == "mixed") {
    mergers.rf <- mergers.rf0
    for(i in 1:length(mergers.rf)) {
      if(i %in% unique(is_long.rf$sample_index)) {
        tmp_index <- is_long.rf$row_index[is_long.rf$sample_index == i]
        if(length(tmp_index) > 0) {
          mergers.rf[[i]]$sequence[tmp_index] <- paste0(
            unmerged.rf[[1]][paste0(is_long.rf$seqID[is_long.rf$sample_index == i], "/1")],
            "NNNNNNNNNN", 
            rc(unmerged.rf[[2]][paste0(is_long.rf$seqID[is_long.rf$sample_index == i], "/2")])
          )
          mergers.rf[[i]]$nmatch[tmp_index] <- 0
          mergers.rf[[i]]$nmismatch[tmp_index] <- 0
          mergers.rf[[i]]$nindel[tmp_index] <- 0
          mergers.rf[[i]]$prefer[tmp_index] <- NA
          mergers.rf[[i]]$accept[tmp_index] <- TRUE
         mergers.rf[[i]] <- mergers.rf[[i]][mergers.rf[[i]]$accept, ]
        }
      } else {
        mergers.rf[[i]] <- mergers.rf[[i]][mergers.rf[[i]]$accept, ]
      }
    }
  }
} else {
  mergers.fr <- lapply(mergers.fr0, function(x) x[x$accept, ])
  if(opt$orientation == "mixed") {
    mergers.rf <- lapply(mergers.rf0, function(x) x[x$accept, ])
  }
}

# Create sequence table
seqtab.fr <- makeSequenceTable(mergers.fr)
if(opt$orientation == "mixed") {
  seqtab.rf <- makeSequenceTable(mergers.rf)
  msg(paste0("There are ", ncol(seqtab.fr), " ASVs in fr orientation, and ", ncol(seqtab.rf), " ASVs in rf orientation after merging.\n"))

  # This is the step at which separate denoising runs should be combined
  # (e.g. if data comes from different sequencer runs or lanes, 
  # or if fwd-rev and rev-fwd orientation were processed separately) 

  # Generate reverse complement of rf
  seqtab.rf.rc <- seqtab.rf
  colnames(seqtab.rf.rc) <- rc(colnames(seqtab.rf))

  # Merge sequence tables
  seqtab_current <- mergeSequenceTables(
    seqtab.fr,
    seqtab.rf.rc,
    repeats = "sum"
  )
} else {
  seqtab_current <- seqtab.fr
}
msg(paste0("The merged sequence table of the current analysis contains ", ncol(seqtab_current), " ASVs.\n"))

# include ASVs from previous data sets
if(opt$prior_seqtab != "none") {
  seqtab_prior <- readRDS(opt$prior_seqtab)
  seqtab <- mergeSequenceTables(
    seqtab_current,
    seqtab_prior,
    repeats = "sum"
  )
} else {
  seqtab <- seqtab_current
}
msg(paste0("The merged sequence table (after including previous data if applicable) contains ", ncol(seqtab), " ASVs.\n"))
save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))

### Chimera removal and further cleaning steps ####

# Remove chimeras
# This may remove quite a bit of ASVs, but only a small fraction of your total sequences
seqtab.nochim <- removeBimeraDenovo(seqtab, method = "consensus", multithread = opt$cpus, verbose = TRUE)
msg(paste0(round(ncol(seqtab.nochim)/ncol(seqtab) * 100), "% of ASVs retained after chimera detection.\n"))
msg("Summary of retained sequences [%] per sample:\n")
round(summary(rowSums(seqtab.nochim)/rowSums(seqtab)) * 100)
save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))

# Sequence length distribution
write.table(
  data.frame(table(rep(nchar(colnames(seqtab.nochim)), colSums(seqtab.nochim)))),
  paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/Length_distribution_", opt$name, ".txt"),
  sep = "\t",
  quote = F,
  row.names = F
)

# Remove potential junk sequences and singletons
# dada does not generate singletons, any singletons are introduced in the merging step
# Adjust range of sequence lengths based on expected length of marker gene fragment
# No need to include a condition for rescuing unmerged reads: without rescue option all reads will be shorter than minimum length required for rescue
seqtab.nochim2 <- seqtab.nochim[
  , 
  (nchar(colnames(seqtab.nochim)) %in% seq(opt$frag_min, opt$frag_max) | nchar(colnames(seqtab.nochim)) > (opt$truncLen_R1 + opt$truncLen_R2 - 10)) & 
    colSums(seqtab.nochim) > 1,
  drop = F
]
msg(paste0("There are ", ncol(seqtab.nochim2), " ASVs in the cleaned sequence count table.\n"))
msg(paste0("This corresponds to ", round(ncol(seqtab.nochim2)/ncol(seqtab) * 100), "% of ASVs compared to the merged data set.\n"))
msg("Summary of retained sequences [%] per sample:\n")
round(summary(rowSums(seqtab.nochim2)/rowSums(seqtab)) * 100)
# write fasta file with removed sequences
seqtab.nochim.junk <- seqtab.nochim[
  ,
  (!nchar(colnames(seqtab.nochim)) %in% seq(opt$frag_min, opt$frag_max) & !nchar(colnames(seqtab.nochim)) > (opt$truncLen_R1 + opt$truncLen_R2 - 10)) |
    colSums(seqtab.nochim) == 1,
  drop = F
]
uniquesToFasta(seqtab.nochim.junk, paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/discarded_", opt$name, ".fasta"))


### Number of sequences at each analysis step ####

# Get number of sequences at each analysis step
nSeq <- read.table(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/nseqs_", opt$name, ".txt"), h = T, stringsAsFactors = F, row.names = 1)
nSeq <- nSeq[sample.names, , drop = F]

getN <- function(x) sum(getUniques(x))
if(opt$orientation == "mixed") {
  if(length(sample.names) == 1) {
    track <- cbind(
      nSeq$Demux,
      filt.out.fr[, 1],
      filt.out.rf[, 1],
      filt.out.fr[, 2],
      filt.out.rf[, 2],
      getN(dadaFs.fr),
      getN(dadaRs.fr),
      getN(dadaFs.rf),
      getN(dadaRs.rf),
      sapply(mergers.fr, getN),
      sapply(mergers.rf, getN),
      # since the seqtab objects may contain data from previous analysis, only select counts from this project by sample names
      rowSums(seqtab.nochim)[sample.names],
      rowSums(seqtab.nochim2)[sample.names]
    )
  } else {
    track <- cbind(
      nSeq$Demux,
      filt.out.fr[, 1],
      filt.out.rf[, 1],
      filt.out.fr[, 2],
      filt.out.rf[, 2],
      sapply(dadaFs.fr, getN), 
      sapply(dadaRs.fr, getN), 
      sapply(dadaFs.rf, getN), 
      sapply(dadaRs.rf, getN), 
      sapply(mergers.fr, getN), 
      sapply(mergers.rf, getN), 
      # since the seqtab objects may contain data from previous analysis, only select counts from this project by sample names
      rowSums(seqtab.nochim)[sample.names], 
      rowSums(seqtab.nochim2)[sample.names]
    )
  }
  colnames(track) <- c(
    "Demux",
    "Clipped_fr",
    "Clipped_rf", 
    "Filtered_fr", 
    "Filtered_rf", 
    "Denoised_fwd_fr", 
    "Denoised_rev_fr", 
    "Denoised_fwd_rf", 
    "Denoised_rev_rf", 
    "Merged_fr",
    "Merged_rf",
    "Nochim",
    "Tabled"
  )
} else {
  if(length(sample.names) == 1) {
    track <- cbind(
      nSeq$Demux,
      filt.out.fr[, 1],
      filt.out.fr[, 2],
      getN(dadaFs.fr),
      getN(dadaRs.fr),
      sapply(mergers.fr, getN),
      rowSums(seqtab.nochim)[sample.names],
      rowSums(seqtab.nochim2)[sample.names]
    )
  } else {
    track <- cbind(
      nSeq$Demux,
      filt.out.fr[, 1],
      filt.out.fr[, 2],
      sapply(dadaFs.fr, getN),
      sapply(dadaRs.fr, getN),
      sapply(mergers.fr, getN),
      rowSums(seqtab.nochim)[sample.names],
      rowSums(seqtab.nochim2)[sample.names]
    )
  }
  colnames(track) <- c(
    "Demux",
    "Clipped_fr",
    "Filtered_fr",
    "Denoised_fwd_fr",
    "Denoised_rev_fr",
    "Merged_fr",
    "Nochim",
    "Tabled"
  )
}
rownames(track) <- c(sample.names)
track <- data.frame(track)


### format sequence names ####

seqtab.nochim2.print <- seqtab.nochim2
colnames(seqtab.nochim2.print) <- paste(opt$prefix, 1:ncol(seqtab.nochim2), sep = "_")


### Write output to file ####
write.table(track, paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/nseqs_all_", opt$name, ".txt"), quote = F, sep = "\t")
write.table(t(seqtab.nochim2.print), paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/asvtab_", opt$name, ".txt"), quote = F, sep = "\t")
uniquesToFasta(seqtab.nochim2, paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/seqs_", opt$name, ".fasta"), ids = colnames(seqtab.nochim2.print))
save.image(paste0(opt$dir, "/Intermediate_results/", opt$analysis, "/workspace_denoising_", opt$name, ".Rdata"))

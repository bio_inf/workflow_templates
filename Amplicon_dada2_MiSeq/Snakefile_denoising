import glob
import pandas as pd

"""
Author: Christiane Hassenrueck
Affiliation: Leibniz Institute for Baltic Sea Research Warnemuende (IOW)
Aim: 
"""

# specify config file
# change this path to the location of the config file with your project specific settings
configfile: "/bio/Common_repositories/workflow_templates/Amplicon_dada2_MiSeq/config/config.yaml"

# snakemake rules

rule all:
	input:
		asvtab = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/asvtab_" + config["project"] + ".txt"

rule dada2_denoising:
	input:
		plot_filt = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/Parameter_screening_" + config["project"] + ".pdf"
	output:
		asvtab = config["adir"] + "/Intermediate_results/" + config["analysis"] + "/asvtab_" + config["project"] + ".txt"
	params:
		script = config["wdir"] + "/scripts/denoising.R",
		truncLen_R1 = config["truncLen_R1"],
		truncLen_R2 = config["truncLen_R2"],
		maxEE_R1 = config["maxEE_R1"],
		maxEE_R2 = config["maxEE_R2"],
		min_len = config["min_len"],
		max_len = config["max_len"],
		errfun = config["errfun"],
		pool = config["pool"],
		rescue = config["rescue"],
		prefix = config["asv_prefix"],
		orientation = config["orientation"],
		prior_seqtab = config["prior_seqtab"],
		project = config["project"],
		sample_list = config["sample_list"],
		adir = config["adir"],
		analysis = config["analysis"]
	conda:
		# config["wdir"] + "/envs/r.yaml"
		"r-4.4.0"
	threads:
		config["parallel_threads"]
	log:
		config["adir"] + "/Intermediate_results/Logfiles/" + config["analysis"] + "/denoising_R.log"
	shell:
		"""
		if [[ "{params.rescue}" != "" ]]
		then
		  if [[ "{params.orientation}"  == "mixed" ]]
		  then
		    {params.script} -n {params.project} -s {params.sample_list} -l {params.truncLen_R1} -L {params.truncLen_R2} -f {params.min_len} -F {params.max_len} -e {params.maxEE_R1} -E {params.maxEE_R2} -m {params.errfun} -P {params.pool} -r {params.rescue} -x {params.prefix} -o {params.orientation} -t {params.prior_seqtab} -d {params.adir} -a {params.analysis} -c {threads} &>> {log}
		  else
		    {params.script} -n {params.project} -s {params.sample_list} -l {params.truncLen_R1} -L {params.truncLen_R2} -f {params.min_len} -F {params.max_len} -e {params.maxEE_R1} -E {params.maxEE_R2} -m {params.errfun} -P {params.pool} -r {params.rescue} -x {params.prefix} -t {params.prior_seqtab} -d {params.adir} -a {params.analysis} -c {threads} &>> {log}
		  fi
		else
		  if [[ "{params.orientation}"  == "mixed" ]]
		  then
		    {params.script} -n {params.project} -s {params.sample_list} -l {params.truncLen_R1} -L {params.truncLen_R2} -f {params.min_len} -F {params.max_len} -e {params.maxEE_R1} -E {params.maxEE_R2} -m {params.errfun} -P {params.pool} -x {params.prefix} -o {params.orientation} -t {params.prior_seqtab} -d {params.adir} -a {params.analysis} -c {threads} &>> {log}
		  else
		    {params.script} -n {params.project} -s {params.sample_list} -l {params.truncLen_R1} -L {params.truncLen_R2} -f {params.min_len} -F {params.max_len} -e {params.maxEE_R1} -E {params.maxEE_R2} -m {params.errfun} -P {params.pool} -x {params.prefix} -t {params.prior_seqtab} -d {params.adir} -a {params.analysis} -c {threads} &>> {log} 
		  fi
		fi
		"""

# workflow_templates

This repository contains a collection of scripts and workflows that may be used as templates for amplicon and shotgun sequence data analysis. It also includes a wiki with further information about the various approaches and collection of links to potentially interesting bioinformatic tools.